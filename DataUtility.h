#ifndef DATA_UTILITY_H
#define DATA_UTILITY_H

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <boost/unordered_map.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/serialization/access.hpp>

using namespace std;
//ASSERT
#define ASSERT(X) {if(!(X)){printf("Failed assert at file: %s, line: %d!",__FILE__,__LINE__);exit(EXIT_FAILURE);}}
//BBox
template <typename T,int DIM=3>
struct BBox {
  typedef Eigen::Matrix<T,DIM,1> PT;
  typedef Eigen::Matrix<T,2,1> PT2;
  BBox() {
    reset();
  }
  virtual ~BBox() {}
  BBox(const PT& p):_minC(p),_maxC(p) {}
  BBox(const PT& minC,const PT& maxC):_minC(minC),_maxC(maxC) {}
  template <typename T2>
  BBox(const BBox<T2,DIM>& other) {
    copy(other);
  }
  template <typename T2>
  BBox& operator=(const BBox<T2,DIM>& other) {
    copy(other);
    return *this;
  }
  static BBox createMM(const PT& minC,const PT& maxC) {
    return BBox(minC,maxC);
  }
  static BBox createME(const PT& minC,const PT& extent) {
    return BBox(minC,minC+extent);
  }
  static BBox createCE(const PT& center,const PT& extent) {
    return BBox(center-extent,center+extent);
  }
  BBox getIntersect(const BBox& other) const {
    return createMM(compMax(_minC,other._minC),compMin(_maxC,other._maxC));
  }
  BBox getUnion(const BBox& other) const {
    return createMM(compMin(_minC,other._minC),compMax(_maxC,other._maxC));
  }
  BBox getUnion(const PT& point) const {
    return createMM(compMin(_minC,point),compMax(_maxC,point));
  }
  BBox getUnion(const PT& ctr,const T& rad) const {
    return createMM(compMin(_minC,ctr-PT::Constant(rad)),compMax(_maxC,ctr+PT::Constant(rad)));
  }
  void setIntersect(const BBox& other) {
    _minC=compMax(_minC,other._minC);
    _maxC=compMin(_maxC,other._maxC);
  }
  void setUnion(const BBox& other) {
    _minC=compMin(_minC,other._minC);
    _maxC=compMax(_maxC,other._maxC);
  }
  void setUnion(const PT& point) {
    _minC=compMin(_minC,point);
    _maxC=compMax(_maxC,point);
  }
  void setUnion(const PT& ctr,const T& rad) {
    _minC=compMin(_minC,ctr-PT::Constant(rad));
    _maxC=compMax(_maxC,ctr+PT::Constant(rad));
  }
  void setPoints(const PT& a,const PT& b,const PT& c) {
    _minC=compMin(compMin(a,b),c);
    _maxC=compMax(compMax(a,b),c);
  }
  PT minCorner() const {
    return _minC;
  }
  PT maxCorner() const {
    return _maxC;
  }
  void enlargedEps(T eps) {
    PT d=(_maxC-_minC)*(eps*0.5f);
    _minC-=d;
    _maxC+=d;
  }
  BBox enlargeEps(T eps) const {
    PT d=(_maxC-_minC)*(eps*0.5f);
    return createMM(_minC-d,_maxC+d);
  }
  void enlarged(T len,const int d=DIM) {
    for(int i=0; i<d; i++) {
      _minC[i]-=len;
      _maxC[i]+=len;
    }
  }
  BBox enlarge(T len,const int d=DIM) const {
    BBox ret=createMM(_minC,_maxC);
    ret.enlarged(len,d);
    return ret;
  }
  PT lerp(const PT& frac) const {
    return _maxC*frac-_minC*(frac-1.0f);
  }
  bool empty() const {
    return !compL(_minC,_maxC);
  }
  template <int DIM2>
  bool containDim(const PT& point) const {
    for(int i=0; i<DIM2; i++)
      if(_minC[i] > point[i] || _maxC[i] < point[i])
        return false;
    return true;
  }
  bool contain(const BBox& other,const int d=DIM) const {
    for(int i=0; i<d; i++)
      if(_minC[i] > other._minC[i] || _maxC[i] < other._maxC[i])
        return false;
    return true;
  }
  bool contain(const PT& point,const int d=DIM) const {
    for(int i=0; i<d; i++)
      if(_minC[i] > point[i] || _maxC[i] < point[i])
        return false;
    return true;
  }
  bool contain(const PT& point,const T& rad,const int d=DIM) const {
    for(int i=0; i<d; i++)
      if(_minC[i]+rad > point[i] || _maxC[i]-rad < point[i])
        return false;
    return true;
  }
  void reset() {
    _minC=PT::Constant( numeric_limits<T>::max());
    _maxC=PT::Constant(-numeric_limits<T>::max());
  }
  PT getExtent() const {
    return _maxC-_minC;
  }
  T distTo(const BBox& other,const int d=DIM) const {
    PT dist=PT::Zero();
    for(int i=0; i<d; i++) {
      if (other._maxC[i] < _minC[i])
        dist[i] = other._maxC[i] - _minC[i];
      else if (other._minC[i] > _maxC[i])
        dist[i] = other._minC[i] - _maxC[i];
    }
    return dist.norm();
  }
  T distTo(const PT& pt,const int d=DIM) const {
    return sqrt(distToSqr(pt,d));
  }
  T distToSqr(const PT& pt,const int d=DIM) const {
    PT dist=PT::Zero();
    for(int i=0; i<d; i++) {
      if (pt[i] < _minC[i])
        dist[i] = pt[i] - _minC[i];
      else if (pt[i] > _maxC[i])
        dist[i] = pt[i] - _maxC[i];
    }
    return dist.squaredNorm();
  }
  PT closestTo(const PT& pt,const int d=DIM) const {
    PT dist(pt);
    for(int i=0; i<d; i++) {
      if (pt[i] < _minC[i])
        dist[i] = _minC[i];
      else if (pt[i] > _maxC[i])
        dist[i] = _maxC[i];
    }
    return dist;
  }
  bool intersect(const PT& p,const PT& q,const int d=DIM) const {
    const T lo=1-5*numeric_limits<T>::min();
    const T hi=1+5*numeric_limits<T>::min();

    T s=0, t=1;
    for(int i=0; i<d; ++i) {
      if(p[i]<q[i]) {
        T d=q[i]-p[i];
        T s0=(_minC[i]-p[i])/d, t0=(_maxC[i]-p[i])/d;
        if(s0>s) s=s0;
        if(t0<t) t=t0;
      } else if(p[i]>q[i]) {
        T d=q[i]-p[i];
        T s0=lo*(_maxC[i]-p[i])/d, t0=hi*(_minC[i]-p[i])/d;
        if(s0>s) s=s0;
        if(t0<t) t=t0;
      } else {
        if(p[i]<_minC[i] || p[i]>_maxC[i])
          return false;
      }

      if(s>t)
        return false;
    }
    return true;
  }
  bool intersect(const PT& p,const PT& q,T& s,T& t,const int d=DIM) const {
    const T lo=1-5*numeric_limits<T>::min();
    const T hi=1+5*numeric_limits<T>::min();

    s=0;
    t=1;
    for(int i=0; i<d; ++i) {
      if(p[i]<q[i]) {
        T d=q[i]-p[i];
        T s0=lo*(_minC[i]-p[i])/d, t0=hi*(_maxC[i]-p[i])/d;
        if(s0>s) s=s0;
        if(t0<t) t=t0;
      } else if(p[i]>q[i]) {
        T d=q[i]-p[i];
        T s0=lo*(_maxC[i]-p[i])/d, t0=hi*(_minC[i]-p[i])/d;
        if(s0>s) s=s0;
        if(t0<t) t=t0;
      } else {
        if(p[i]<_minC[i] || p[i]>_maxC[i])
          return false;
      }

      if(s>t)
        return false;
    }
    return true;
  }
  bool intersect(const BBox& other,const int& d=DIM) const {
    for(int i=0; i<d; i++) {
      if(_maxC[i] < other._minC[i] || other._maxC[i] < _minC[i])
        return false;
    }
    return true;
    //return compLE(_minC,other._maxC) && compLE(other._minC,_maxC);
  }
  PT2 project(const PT& a,const int d=DIM) const {
    PT ctr=(_minC+_maxC)*0.5f;
    T ctrD=a.dot(ctr);
    T delta=0.0f;
    ctr=_maxC-ctr;
    for(int i=0; i<d; i++)
      delta+=abs(ctr[i]*a[i]);
    return PT2(ctrD-delta,ctrD+delta);
  }
  template<typename T2>
  BBox& copy(const BBox<T2,DIM>& other) {
    for(int i=0; i<DIM; i++) {
      _minC[i]=other._minC[i];
      _maxC[i]=other._maxC[i];
    }
    return *this;
  }
  T perimeter(const int d=DIM) const {
    PT ext=getExtent();
    if(d <= 2)
      return ext.sum()*2.0f;
    else {
      ASSERT(d == 3);
      return (ext[0]*ext[1]+ext[1]*ext[2]+ext[0]*ext[2])*2.0f;
    }
  }
  PT _minC;
  PT _maxC;
};
//VTK compatible atomic IO
void vtkWrite(ostream& oss,float val);
void vtkWrite(ostream& oss,double val);
void vtkWrite(ostream& oss,int val);
void vtkWrite(ostream& oss,unsigned char val);
template <typename T>
struct VTKWriter {
  enum VTK_DATA_TYPE {
    UNSTRUCTURED_GRID,
    STRUCTURED_POINTS,
  };
  enum VTK_CELL_TYPE {
    POINT=1,
    LINE=3,
    TRIANGLE=5,
    TETRA=10,
    VOXEL=11,
    HEX=12,
    POLYLINE=4,
    QUAD=9,
    QUADRATIC_LINE=21,
  };
public:
  struct Data {
    Data():_nr(0) {}
    string _str;
    int _nr;
  };
  template <typename ITER>
  struct ValueTraits {
    typedef typename ITER::value_type value_type;
  };
  template <typename POINTED_TYPE>
  struct ValueTraits<POINTED_TYPE*> {
    typedef POINTED_TYPE value_type;
  };
  template <typename ID>
  struct IteratorIndex {
    typedef ID value_type;
    IteratorIndex(const int& id,const int stride,const int& off)
      :_id(id),_stride(stride),_off(off) {}
    void operator++() {
      _id++;
    }
    bool operator!=(const IteratorIndex& other) const {
      return _id < other._id;
    }
    virtual ID operator*() const {
      ID ret;
      for(int i=0; i<ret.size(); i++)ret(i)=(_stride == 0) ? _id+_off*i : _id*_stride+i;
      return ret;
    }
    int _id;
    int _stride;
    int _off;
  };
  template <typename ID>
  struct IteratorRepeat {
    typedef ID value_type;
    IteratorRepeat(const int& id,const ID& val)
      :_id(id),_val(val) {}
    void operator++() {
      _id++;
    }
    bool operator!=(const IteratorRepeat& other) const {
      return _id < other._id;
    }
    virtual ID operator*() const {
      return _val;
    }
    int _id;
    ID _val;
  };
  template <typename ITER>
  struct IteratorAdd {
    typedef typename ValueTraits<ITER>::value_type value_type;
    IteratorAdd(ITER beg0,ITER beg1):_beg0(beg0),_beg1(beg1) {}
    void operator++() {
      _beg0++;
      _beg1++;
    }
    bool operator!=(const IteratorAdd& other) const {
      return _beg0 != other._beg0;
    }
    virtual value_type operator*() const {
      return (*_beg0)+(*_beg1);
    }
    ITER _beg0,_beg1;
  };
  template <typename ITER,typename SCALAR>
  struct IteratorAddMult {
    typedef typename ValueTraits<ITER>::value_type value_type;
    IteratorAddMult(ITER beg0,ITER beg1,SCALAR mult):_beg0(beg0),_beg1(beg1),_mult(mult) {}
    void operator++() {
      _beg0++;
      _beg1++;
    }
    bool operator!=(const IteratorAddMult& other) const {
      return _beg0 != other._beg0;
    }
    virtual value_type operator*() const {
      return (*_beg0)+(*_beg1)*_mult;
    }
    ITER _beg0,_beg1;
    SCALAR _mult;
  };
public:
  VTKWriter(const string& name,const boost::filesystem::path& path,bool binary)
    :_os(path,binary ? ios_base::binary : ios_base::out),
     _points(binary ? ios_base::binary : ios_base::out),
     _cells(binary ? ios_base::binary : ios_base::out),
     _cellTypes(binary ? ios_base::binary : ios_base::out),
     _cellDatas(binary ? ios_base::binary : ios_base::out),
     _nrPoint(0),_nrCell(0),_nrIndex(0),_nrData(0),_vdt(UNSTRUCTURED_GRID),
     _binary(binary) {
    _os << "# vtk DataFile Version 1.0" << endl;
    _os << name << endl;
    _os << (binary ? "BINARY" : "ASCII") << endl;
    _os << "DATASET " << "UNSTRUCTURED_GRID" << endl;
  }
  VTKWriter(const string& name,const boost::filesystem::path& path,bool binary,const BBox<T>& bb,const Eigen::Vector3i& nrCell,bool center)
    :_os(path,binary ? ios_base::binary : ios_base::out),
     _points(binary ? ios_base::binary : ios_base::out),
     _cells(binary ? ios_base::binary : ios_base::out),
     _cellTypes(binary ? ios_base::binary : ios_base::out),
     _cellDatas(binary ? ios_base::binary : ios_base::out),
     _nrPoint(0),_nrCell(0),_nrIndex(0),_nrData(0),_vdt(STRUCTURED_POINTS),
     _binary(binary) {
    typename BBox<T>::PT ext=bb.getExtent();
    typename BBox<T>::PT spacing(ext.x()/nrCell.x(),ext.y()/nrCell.y(),ext.z()/nrCell.z());
    _os << "# vtk DataFile Version 1.0" << endl;
    _os << name << endl;
    _os << (binary ? "BINARY" : "ASCII") << endl;
    _os << "DATASET " << "STRUCTURED_POINTS" << endl;
    if(center) {
      typename BBox<T>::PT origin=bb._minC+spacing*0.5f;
      _os << "DIMENSIONS " << nrCell.x() << " " << nrCell.y() << " " << nrCell.z() << endl;
      _os << "ORIGIN " << origin.x() << " " << origin.y() << " " << origin.z() << endl;
      _os << "SPACING " << spacing.x() << " " << spacing.y() << " " << spacing.z() << endl;
    } else {
      typename BBox<T>::PT origin=bb._minC;
      _os << "DIMENSIONS " << (nrCell.x()+1) << " " << (nrCell.y()+1) << " " << (nrCell.z()+1) << endl;
      _os << "ORIGIN " << origin.x() << " " << origin.y() << " " << origin.z() << endl;
      _os << "SPACING " << spacing.x() << " " << spacing.y() << " " << spacing.z() << endl;
    }
  }
  virtual ~VTKWriter() {
    bool first;
    switch(_vdt) {
    case UNSTRUCTURED_GRID:
      _os << "POINTS " << _nrPoint << " " << (sizeof(T) == sizeof(float) ? "float" : "double") << endl;
      _os << _points.str();
      _os << "CELLS " << _nrCell << " " << _nrIndex << endl;
      _os << _cells.str();
      _os << "CELL_TYPES " << _nrCell << endl;
      _os << _cellTypes.str();
      first=false;
      for(typename boost::unordered_map<string,Data>::const_iterator beg=_customData.begin(),end=_customData.end(); beg!=end; beg++) {
        if(!first)
          _os << "CELL_DATA " << beg->second._nr << endl;
        first=true;
        _os << beg->second._str << endl;
      }
      first=false;
      for(typename boost::unordered_map<string,Data>::const_iterator beg=_customPointData.begin(),end=_customPointData.end(); beg!=end; beg++) {
        if(!first)
          _os << "POINT_DATA " << beg->second._nr << endl;
        first=true;
        _os << beg->second._str << endl;
      }
      break;
    case STRUCTURED_POINTS:
      ASSERT(_nrData == 1);
      _os << "POINT_DATA " << _nrPoint << std::endl;
      _os << "SCALARS data " << (sizeof(T) == sizeof(float) ? "float" : "double") << endl;
      _os << "LOOKUP_TABLE default" << endl;
      _os << _cellDatas.str();
      break;
    default:
      printf("Unsupported!");
      ASSERT(false);
    }
  }
  template <typename ITER> VTKWriter& appendPoints(ITER beg,ITER end) {
    typedef typename ValueTraits<ITER>::value_type value_type;
    int nr=0;
    if(_binary) {
      for(; beg != end; ++beg) {
        const value_type& val=*beg;
        vtkWrite(_points,val(0));
        vtkWrite(_points,val(1));
        vtkWrite(_points,val(2));
        nr++;
      }
    } else {
      for(; beg != end; ++beg) {
        const value_type& val=*beg;
        _points << (T)val(0) << " " << (T)val(1) << " " << (T)val(2) << endl;
        nr++;
      }
    }
    _nrPoint+=nr;
    return *this;
  }
  template <typename ITER> VTKWriter& appendVoxels(ITER beg,ITER end,bool hex) {
    typedef typename Eigen::Matrix<int,8,1> IDS;
    typedef typename ValueTraits<ITER>::value_type value_type;
    std::vector<value_type,Eigen::aligned_allocator<value_type> > points;
    std::vector<IDS,Eigen::aligned_allocator<IDS> > cells;
    for(; beg!=end;) {
      IDS ids;
      value_type minC=*beg++;
      value_type maxC=*beg++;
      value_type ext=maxC-minC;

      if(hex) ids << 0,1,3,2,4,5,7,6;
      else ids << 0,1,2,3,4,5,6,7;
      ids.array()+=points.size();
      cells.push_back(ids);

      points.push_back(minC+value_type(0.0f   ,0.0f   ,0.0f   ));
      points.push_back(minC+value_type(ext.x(),   0.0f,0.0f   ));
      points.push_back(minC+value_type(0.0f   ,ext.y(),0.0f   ));
      points.push_back(minC+value_type(ext.x(),ext.y(),0.0f   ));

      points.push_back(minC+value_type(0.0f   ,0.0f   ,ext.z()));
      points.push_back(minC+value_type(ext.x(),   0.0f,ext.z()));
      points.push_back(minC+value_type(0.0f   ,ext.y(),ext.z()));
      points.push_back(minC+value_type(ext.x(),ext.y(),ext.z()));
    }
    setRelativeIndex();
    appendPoints(points.begin(),points.end());
    appendCells(cells.begin(),cells.end(),hex ? HEX : VOXEL,true);
    return *this;
  }
  template <typename ITER> VTKWriter& appendCells(ITER beg,ITER end,VTK_CELL_TYPE ct,bool relativeIndex=false) {
    if(relativeIndex)
      ASSERT(_relativeCellIndex >= -1);
    int base=relativeIndex ? (int)_relativeCellIndex : 0;

    typedef typename ValueTraits<ITER>::value_type value_type;
    int nr=0;
    int nrIndex=0;
    if(_binary) {
      for(; beg != end; ++beg) {
        const value_type& val=*beg;
        switch(ct) {
        case POINT:
          nrIndex+=2;
          vtkWrite(_cells,1);
          vtkWrite(_cells,base+(int)val(0));
          break;
        case LINE:
          nrIndex+=3;
          vtkWrite(_cells,2);
          vtkWrite(_cells,base+(int)val(0));
          vtkWrite(_cells,base+(int)val(1));
          break;
        case TRIANGLE:
        case QUADRATIC_LINE:
          nrIndex+=4;
          vtkWrite(_cells,3);
          vtkWrite(_cells,base+(int)val(0));
          vtkWrite(_cells,base+(int)val(1));
          vtkWrite(_cells,base+(int)val(2));
          break;
        case TETRA:
        case QUAD:
          nrIndex+=5;
          vtkWrite(_cells,4);
          vtkWrite(_cells,base+(int)val(0));
          vtkWrite(_cells,base+(int)val(1));
          vtkWrite(_cells,base+(int)val(2));
          vtkWrite(_cells,base+(int)val(3));
          break;
        case VOXEL:
        case HEX:
          nrIndex+=9;
          vtkWrite(_cells,8);
          vtkWrite(_cells,base+(int)val(0));
          vtkWrite(_cells,base+(int)val(1));
          vtkWrite(_cells,base+(int)val(2));
          vtkWrite(_cells,base+(int)val(3));
          vtkWrite(_cells,base+(int)val(4));
          vtkWrite(_cells,base+(int)val(5));
          vtkWrite(_cells,base+(int)val(6));
          vtkWrite(_cells,base+(int)val(7));
          break;
        case POLYLINE:
          nrIndex+=(int)val.rows()+1;
          vtkWrite(_cells,(int)val.rows());
          for(int i=0; i<(int)val.rows(); i++)
            vtkWrite(_cells,base+(int)val[i]);
          break;
        }
        vtkWrite(_cellTypes,(int)ct);
        nr++;
      }
    } else {
      for(; beg != end; ++beg) {
        const value_type& val=*beg;
        switch(ct) {
        case POINT:
          nrIndex+=2;
          _cells << "1 " << (base+(int)val(0)) << endl;
          break;
        case LINE:
          nrIndex+=3;
          _cells << "2 " << (base+(int)val(0)) << " " << (base+(int)val(1)) << endl;
          break;
        case TRIANGLE:
        case QUADRATIC_LINE:
          nrIndex+=4;
          _cells << "3 " << (base+(int)val(0)) << " " << (base+(int)val(1)) << " " << (base+(int)val(2)) << endl;
          break;
        case TETRA:
        case QUAD:
          nrIndex+=5;
          _cells << "4 " << (base+(int)val(0)) << " " << (base+(int)val(1)) << " " << (base+(int)val(2)) << " " << (base+(int)val(3)) << endl;
          break;
        case VOXEL:
        case HEX:
          nrIndex+=9;
          _cells << "8 " << (base+(int)val(0)) << " " << (base+(int)val(1)) << " " << (base+(int)val(2)) << " " << (base+(int)val(3)) << " "
                 << (base+(int)val(4)) << " " << (base+(int)val(5)) << " " << (base+(int)val(6)) << " " << (base+(int)val(7)) << endl;
          break;
        case POLYLINE:
          nrIndex+=(int)val.rows()+1;
          _cells << val.rows() << " ";
          for(int i=0; i<(int)val.rows(); i++)
            _cells << (base+(int)val[i]) << " ";
          _cells << endl;
          break;
        }
        _cellTypes << ct << endl;
        nr++;
      }
    }
    _nrCell+=nr;
    _nrIndex+=nrIndex;
    return *this;
  }
  template <typename ITER> VTKWriter& appendDatas(const string name,ITER beg,ITER end) {
    if(_binary)
      for(; beg != end; ++beg,++_nrPoint)
        vtkWrite(_cellDatas,*beg);
    else
      for(; beg != end; ++beg,++_nrPoint)
        _cellDatas << (T)*beg << endl;
    _nrData++;
    return *this;
  }
  template <typename ITER> VTKWriter& appendCustomData(const string name,ITER beg,ITER end) {
    ostringstream os;
    if(_customData.find(name) == _customData.end()) {
      os << "SCALARS " << name << " " << (sizeof(T) == sizeof(float) ? "float" : "double") << endl;
      os << "LOOKUP_TABLE default" << endl;
    }

    Data& dat=_customData[name];
    if(_binary)
      for(; beg != end; ++beg,++dat._nr)
        vtkWrite(os,(T)*beg);
    else
      for(; beg != end; ++beg,++dat._nr)
        os << (T)*beg << endl;
    dat._str+=os.str();
    ASSERT(dat._nr == _nrCell);
    return *this;
  }
  template <typename ITER> VTKWriter& appendCustomPointData(const string name,ITER beg,ITER end) {
    ostringstream os;
    if(_customPointData.find(name) == _customPointData.end()) {
      //Data& dat=_customPointData[name];
      os << "SCALARS " << name << " " << (sizeof(T) == sizeof(float) ? "float" : "double") << endl;
      os << "LOOKUP_TABLE default" << endl;
    }

    Data& dat=_customPointData[name];
    if(_binary)
      for(; beg != end; ++beg,++dat._nr)
        vtkWrite(os,(T)*beg);
    else
      for(; beg != end; ++beg,++dat._nr)
        os << (T)*beg << endl;
    dat._str+=os.str();
    ASSERT(dat._nr == _nrPoint);
    return *this;
  }
  template <typename ITER> VTKWriter& appendCustomVectorData(const string name,ITER beg,ITER end) {
    ostringstream os;
    if(_customData.find(name) == _customData.end()) {
      os << "VECTORS " << name << " " << (sizeof(T) == sizeof(float) ? "float" : "double") << endl;
    }

    Data& dat=_customData[name];
    if(_binary)
      for(; beg != end; ++beg,++dat._nr) {
        vtkWrite(os,(T)(*beg)[0]);
        vtkWrite(os,(T)(*beg)[1]);
        vtkWrite(os,(T)(*beg)[2]);
      }
    else
      for(; beg != end; ++beg,++dat._nr)
        os << (T)(*beg)[0] << " " << (T)(*beg)[1] << " " << (T)(*beg)[2] << endl;
    dat._str+=os.str();
    ASSERT(dat._nr == _nrCell);
    return *this;
  }
  template <typename ITER> VTKWriter& appendCustomPointVectorData(const string name,ITER beg,ITER end) {
    ostringstream os;
    if(_customPointData.find(name) == _customPointData.end()) {
      //Data& dat=_customPointData[name];
      os << "VECTORS " << name << " " << (sizeof(T) == sizeof(float) ? "float" : "double") << endl;
    }

    Data& dat=_customPointData[name];
    if(_binary)
      for(; beg != end; ++beg,++dat._nr) {
        vtkWrite(os,(T)(*beg)[0]);
        vtkWrite(os,(T)(*beg)[1]);
        vtkWrite(os,(T)(*beg)[2]);
      }
    else
      for(; beg != end; ++beg,++dat._nr)
        os << (T)(*beg)[0] << " " << (T)(*beg)[1] << " " << (T)(*beg)[2] << endl;
    dat._str+=os.str();
    ASSERT(dat._nr == _nrPoint);
    return *this;
  }
  template <typename ITER> VTKWriter& appendPointsByAdd(ITER beg0,ITER beg1,ITER end0) {
    appendPoints(IteratorAdd<ITER>(beg0,beg1),IteratorAdd<ITER>(end0,end0));
    return *this;
  }
  template <typename ITER> VTKWriter& appendCustomPointColorData(const string name,ITER begC,ITER endC) {
    ostringstream os;
    if(_customPointData.find(name) == _customPointData.end()) {
      //Data& dat=_customPointData[name];
      os << "COLOR_SCALARS " << name << " " << (*begC).size() << endl;
    }

    int sz=(int)(*begC).size();
    Data& dat=_customPointData[name];
    if(_binary) {
      for(; begC != endC; ++begC,++dat._nr)
        for(int d=0; d<sz; d++)
          vtkWrite(os,(unsigned char)((*begC)[d]*255));
    } else {
      for(; begC != endC; ++begC,++dat._nr) {
        for(int d=0; d<sz; d++)
          os << (T)(*begC)[d] << " ";
        os << endl;
      }
    }
    dat._str+=os.str();
    return *this;
  }
  void setRelativeIndex(int rel=-1) {
    if(rel == -1)
      _relativeCellIndex=_nrPoint;
    else _relativeCellIndex=rel;
  }
private:
  boost::filesystem::ofstream _os;
  ostringstream _points,_cells,_cellTypes,_cellDatas;
  boost::unordered_map<string,Data> _customData;
  boost::unordered_map<string,Data> _customPointData;
  int _nrPoint,_nrCell,_nrIndex,_nrData;
  VTK_DATA_TYPE _vdt;
  bool _binary;
  int _relativeCellIndex;
};
//Eigen serialize
namespace boost
{
namespace serialization
{
template<class Archive,int n,int m>
void serialize(Archive& ar,Eigen::Matrix<double,n,m>& g, const unsigned int version)
{
  size_t rows=g.rows();
  size_t cols=g.cols();
  ar & rows & cols;
  if(g.rows() != rows || g.cols() != cols)
    g.resize(rows,cols);
  for(int c=0; c<g.cols(); c++)
    for(int r=0; r<g.rows(); r++)
      ar & g(r,c);
}
template<class Archive,int n,int m>
void serialize(Archive& ar,Eigen::Matrix<int,n,m>& g, const unsigned int version)
{
  size_t rows=g.rows();
  size_t cols=g.cols();
  ar & rows & cols;
  if(g.rows() != rows || g.cols() != cols)
    g.resize(rows,cols);
  for(int c=0; c<g.cols(); c++)
    for(int r=0; r<g.rows(); r++)
      ar & g(r,c);
}
template<class Archive>
void serialize(Archive& ar,Eigen::Affine3d& g, const unsigned int version)
{
  for(int c=0; c<3; c++) {
    ar & g.translation()[c];
    for(int r=0; r<3; r++)
      ar & g.linear()(r,c);
  }
}
}
}

#endif
