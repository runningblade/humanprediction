#ifndef CRF_H
#define CRF_H

#include "DataStructure.h"

//optimization problem definition
struct LPCoef {
  LPCoef(double coef=0);
  LPCoef(double coef,int off,const Eigen::VectorXd& feature);
  LPCoef& operator+=(const LPCoef& other);
  double _coef;
  int _weightOffset;
  Eigen::VectorXd _feature;
};
struct LPProb {
  double eval(const Eigen::VectorXd& Y) const;
  //linear objective
  vector<LPCoef> _coef;
  //general linear constraints
  vector<Eigen::Triplet<double,int> > _trips;
  vector<double> _rhs;
  vector<int> _type;
  //quadratic boolean terms
  map<int,map<int,LPCoef> > _quadraticTerms;
  //names of variables
  map<int,string> _nameBinding;
  //the result is stored here
  Eigen::VectorXd _result;
  //loss function used by QPBOLib for SVM-struct learning
  vector<double> _Y;
  //options
  enum VAR_TYPE {BINARY,CONTINUOUS01,CONTINUOUS};
  VAR_TYPE _varType;
};
struct QPProb {
  //we want to minimize [w,C]^T*_A*[w,C]/2+[w,C]^T*_b
  Eigen::DiagonalMatrix<double,-1> _A;
  Eigen::VectorXd _b;
  //we want _cons[i].dot([w,C]) >= b[i] for all i
  vector<Eigen::SparseVector<double> > _cons;
  vector<double> _rhs;
  //the result can be found here
  Eigen::VectorXd _result;
};
//The CRF Node and Edge
struct CRF {
  typedef map<string,pair<Eigen::VectorXd,int> > VertexWeight;
  typedef map<string,map<string,pair<Eigen::VectorXd,int> > > EdgeWeight;
  struct Term {
    Term(const Activity* act):_act(act) {}
    virtual pair<Eigen::VectorXd,int> calcFeature() const =0;
    virtual void buildLPIndex(LPProb& lp,map<int,string>& nameBinding){}
    virtual void buildLPProb(LPProb& lp) const =0;
    virtual void assignLPResult(const Eigen::VectorXd& result) {}
    const Activity* _act;
  };
  struct OOTerm : public Term {
    OOTerm(const Activity* act,const EdgeWeight& w,const SubActivity* segment,int obj1,int obj2);
    virtual pair<Eigen::VectorXd,int> calcFeature() const;
    virtual void buildLPProb(LPProb& lp) const;
    Eigen::Vector4d calcFeature(int f) const;
    static int nrFeats();
    const EdgeWeight& _w;
    const SubActivity* _segment;
    const int _obj1,_obj2;
  };
  struct OATerm : public Term {
    OATerm(const Activity* act,const EdgeWeight& w,const SubActivity* segment,int obj);
    virtual pair<Eigen::VectorXd,int> calcFeature() const;
    virtual void buildLPProb(LPProb& lp) const;
    Eigen::Matrix<double,8,1> calcFeature(int frm) const;
    static int nrFeats();
    const EdgeWeight& _w;
    const SubActivity* _segment;
    const int _obj;
  };
  struct OOTTerm : public Term {
    OOTTerm(const Activity* act,const EdgeWeight& w,const SubActivity* segment1,const SubActivity* segment2,int obj);
    virtual pair<Eigen::VectorXd,int> calcFeature() const;
    virtual void buildLPProb(LPProb& lp) const;
    static int nrFeats();
    const EdgeWeight& _w;
    const SubActivity* _segment1;
    const SubActivity* _segment2;
    const int _obj;
  };
  struct AATTerm : public Term {
    AATTerm(const Activity* act,const EdgeWeight& w,const SubActivity* segment1,const SubActivity* segment2);
    virtual pair<Eigen::VectorXd,int> calcFeature() const;
    virtual void buildLPProb(LPProb& lp) const;
    static int nrFeats();
    const EdgeWeight& _w;
    const SubActivity* _segment1;
    const SubActivity* _segment2;
  };
  struct OTerm : public Term {
    OTerm(const Activity* act,const VertexWeight& w,const SubActivity* segment,int obj);
    virtual pair<Eigen::VectorXd,int> calcFeature() const;
    virtual void buildLPIndex(LPProb& lp,map<int,string>& nameBinding);
    virtual void buildLPProb(LPProb& lp) const;
    virtual void assignLPResult(const Eigen::VectorXd& result);
    Eigen::Matrix<double,13,1> calcFeature(int frm) const;
    static int nrFeats();
    const VertexWeight& _w;
    const SubActivity* _segment;
    const int _obj;
  };
  struct ATerm : public Term {
    ATerm(const Activity* act,const VertexWeight& w,const SubActivity* segment);
    virtual pair<Eigen::VectorXd,int> calcFeature() const;
    virtual void buildLPIndex(LPProb& lp,map<int,string>& nameBinding);
    virtual void buildLPProb(LPProb& lp) const;
    virtual void assignLPResult(const Eigen::VectorXd& result);
    Eigen::Matrix<double,24,1> calcFeature(int frm) const;
    Eigen::Matrix<double,47,1> calcPoseFeature(int frm) const;
    Eigen::Matrix<double,16,1> calcHandFeature(int frm,vector<Eigen::Vector2d>& handVerticalPos,int& denom) const;
    static int nrFeats();
    const VertexWeight& _w;
    const SubActivity* _segment;
  };
  void setZero();
  void setRandom();
  void assign(const Eigen::VectorXd& ret);
  Eigen::VectorXd assemble() const;
  void init(const Activities& acts);
  void predicate(const Activity& act);
  void train(double C,double eps,const string& path,const Activities& acts);
  void read(const string& path);
  void write(const string& path);
  template<class Archive>
  void serialize(Archive& ar,const unsigned int version) {
    ar & _nrW & _O & _A & _OO & _OA & _OOT & _AAT;
  }
//private:
  void addActivity(const Activity& act,vector<boost::shared_ptr<Term> >& terms);
  void buildTrainingSamples(vector<LPProb>& trainingSamples, const Activities& acts);
  int _nrW;
  VertexWeight _O,_A;
  EdgeWeight _OO,_OA,_OOT,_AAT;
  vector<boost::shared_ptr<Term> > _terms;
};
//Linear Programming Interface
struct LP {
  //the interface
  static void solveGraphCutQPBO(LPProb& lp);
  static void solveGraphCut(LPProb& lp,ostream* os);
  static void solveGLPK(LPProb& lp,ostream* os);
  static void relaxQP(LPProb& lp);
  //build from CRF node energy
  static void buildLPProbCRF(LPProb& lp,const CRF::VertexWeight& w,const Eigen::VectorXd& f,int index);
  //build from CRF edge energy
  static void buildLPProbCRF(LPProb& lp,const CRF::EdgeWeight& w,const Eigen::VectorXd& f,int indexY1,int indexY2);
  //run example
  static void runExample();
};
//Quadratic Programming Interface
struct QP {
  //the interface
  static void solveQPPrimal(QPProb& qp);
  static void solveQPDual(QPProb& qp);
  //run example
  static void runExample();
};

#endif
