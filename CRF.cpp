#include "CRF.h"
#include "glpk.h"
#include "QPBO.h"
#include "alglib/optimization.h"
#include "constants.h"
#include <boost/lexical_cast.hpp>
//#include <boost/archive/text_oarchive.hpp>
//#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/map.hpp>
#define ITERV(E,IT) for(VertexWeight::IT beg=E.begin(),end=E.end();beg!=end;beg++)
#define ITERE(E,IT) for(EdgeWeight::IT beg2=E.begin(),end2=E.end();beg2!=end2;beg2++)for(VertexWeight::IT beg=beg2->second.begin(),end=beg2->second.end();beg!=end;beg++)
#define ZEROV(E) ITERV(E,iterator)beg->second.first.setZero();
#define ZEROE(E) ITERE(E,iterator)beg->second.first.setZero();
#define RANDOMV(E) ITERV(E,iterator)beg->second.first.setRandom();
#define RANDOME(E) ITERE(E,iterator)beg->second.first.setRandom();
#define ASSIGNV(E) ITERV(E,iterator)beg->second.first=ret.block(beg->second.second,0,beg->second.first.size(),1);
#define ASSIGNE(E) ITERE(E,iterator)beg->second.first=ret.block(beg->second.second,0,beg->second.first.size(),1);
#define ASSEMBLEV(E) ITERV(E,const_iterator)ret.block(beg->second.second,0,beg->second.first.size(),1)=beg->second.first;
#define ASSEMBLEE(E) ITERE(E,const_iterator)ret.block(beg->second.second,0,beg->second.first.size(),1)=beg->second.first;

#include <time.h>
#include <sys/time.h>
static double get_wall_time() {
  struct timeval time;
  if (gettimeofday(&time,NULL)) {
    //  Handle error
    return 0;
  }
  return (double)time.tv_sec + (double)time.tv_usec * .000001;
}

//LPCoef
LPCoef::LPCoef(double coef):_coef(coef),_weightOffset(-1){}
LPCoef::LPCoef(double coef,int off,const Eigen::VectorXd& feature):_coef(coef),_weightOffset(off),_feature(feature){}
LPCoef& LPCoef::operator+=(const LPCoef& other) {
  _coef+=other._coef;
  if(_weightOffset == -1 && other._weightOffset != -1) {
    _weightOffset=other._weightOffset;
    _feature=other._feature;
  } else if(_weightOffset != -1 && other._weightOffset != -1)
    ASSERT(_weightOffset == other._weightOffset && _feature == other._feature)
  return *this;
}
double LPProb::eval(const Eigen::VectorXd& Y) const
{
  double ret=0;
  for(int i=0;i<(int)_coef.size();i++)
    ret+=_coef[i]._coef*Y[i];
  for(map<int,map<int,LPCoef> >::const_iterator beg2=_quadraticTerms.begin(),end2=_quadraticTerms.end();beg2!=end2;beg2++)
    for(map<int,LPCoef>::const_iterator beg=beg2->second.begin(),end=beg2->second.end();beg!=end;beg++)
      ret+=beg->second._coef*Y[beg2->first]*Y[beg->first];
  for(int i=0;i<(int)_Y.size();i++)
    ret+=abs(_Y[i]-Y[i]);
  return ret;
}
//OOTerm: consistent with paper
CRF::OOTerm::OOTerm(const Activity* act,const EdgeWeight& w,const SubActivity* segment,int obj1,int obj2)
  :Term(act),_w(w),_segment(segment),_obj1(obj1),_obj2(obj2) {}
pair<Eigen::VectorXd,int> CRF::OOTerm::calcFeature() const
{
  pair<Eigen::VectorXd,int> ret=_w.find(_segment->_affordance[_obj1])->second.find(_segment->_affordance[_obj2])->second;
  int start=_segment->_start;
  int end=_segment->_end;
  vector<Eigen::Vector4d> feats(end-start+1);
  for(int i=start; i<=end; i++)
    feats[i-start]=calcFeature(i);
  ret.first.block<4,1>(0,0)=feats[start-start];
  ret.first.block<4,1>(4,0)=feats[end-start];
  ret.first.block<4,1>(8,0)=feats[(start+end)/2-start];
  ret.first.block<4,1>(12,0)=feats[start-start];
  ret.first.block<4,1>(16,0)=feats[start-start];
  for(int f=start,t=end; f<=t; f++) {
    ret.first.block<4,1>(12,0)=ret.first.block<4,1>(12,0).cwiseMax(feats[f-start]);
    ret.first.block<4,1>(16,0)=ret.first.block<4,1>(16,0).cwiseMin(feats[f-start]);
  }
  for(int f=start,t=end; f<=t; f++)
    for(int j=0; j<4; j++) {
      ASSERT(ret.first[12+j] >= feats[f-start][j] && ret.first[12+j] >= feats[f-start][j] && ret.first[12+j] >= feats[f-start][j])
      ASSERT(ret.first[16+j] <= feats[f-start][j] && ret.first[16+j] <= feats[f-start][j] && ret.first[16+j] <= feats[f-start][j])
    }
  return ret;
}
void CRF::OOTerm::buildLPProb(LPProb& lp) const
{
  Eigen::VectorXd f=calcFeature().first;
  int Y1=_segment->_LPIndexAffordance[_obj1];
  int Y2=_segment->_LPIndexAffordance[_obj2];
  LP::buildLPProbCRF(lp,_w,f,Y1,Y2);
}
Eigen::Vector4d CRF::OOTerm::calcFeature(int f) const
{
  const ObjectFeature& feat1=_act->_objects.find(Object(_obj1))->_features.find(f)->second;
  const ObjectFeature& feat2=_act->_objects.find(Object(_obj2))->_features.find(f)->second;
  Eigen::Vector4d ret;
  ret.block<3,1>(0,0)=feat1._centroid-feat2._centroid;
  ret[3]=(feat1._centroid-feat2._centroid).norm();
  return ret;
}
int CRF::OOTerm::nrFeats()
{
  return 20;
}
//OATerm: consistent with paper
CRF::OATerm::OATerm(const Activity* act,const EdgeWeight& w,const SubActivity* segment,int obj)
  :Term(act),_w(w),_segment(segment),_obj(obj) {}
pair<Eigen::VectorXd,int> CRF::OATerm::calcFeature() const
{
  pair<Eigen::VectorXd,int> ret=_w.find(_segment->_affordance[_obj])->second.find(_segment->_type)->second;
  int start=_segment->_start;
  int end=_segment->_end;
  vector<Eigen::Matrix<double,8,1> > feats(end-start+1);
  for(int i=start; i<=end; i++)
    feats[i-start]=calcFeature(i);
  ret.first.block<8,1>(0,0)=feats[start-start];
  ret.first.block<8,1>(8,0)=feats[end-start];
  ret.first.block<8,1>(16,0)=feats[(start+end)/2-start];
  ret.first.block<8,1>(24,0)=feats[start-start];
  ret.first.block<8,1>(32,0)=feats[start-start];
  for(int f=start,t=end; f<=t; f++) {
    ret.first.block<8,1>(24,0)=ret.first.block<8,1>(24,0).cwiseMax(feats[f-start]);
    ret.first.block<8,1>(32,0)=ret.first.block<8,1>(32,0).cwiseMin(feats[f-start]);
  }
  for(int f=start,t=end; f<=t; f++)
    for(int j=0; j<8; j++) {
      ASSERT(ret.first[24+j] >= feats[f-start][j] && ret.first[24+j] >= feats[f-start][j] && ret.first[24+j] >= feats[f-start][j])
      ASSERT(ret.first[32+j] <= feats[f-start][j] && ret.first[32+j] <= feats[f-start][j] && ret.first[32+j] <= feats[f-start][j])
    }
  return ret;
}
void CRF::OATerm::buildLPProb(LPProb& lp) const
{
  Eigen::VectorXd f=calcFeature().first;
  int Y1=_segment->_LPIndexAffordance[_obj];
  int Y2=_segment->_LPIndexType;
  LP::buildLPProbCRF(lp,_w,f,Y1,Y2);
}
Eigen::Matrix<double,8,1> CRF::OATerm::calcFeature(int frm) const
{
  Eigen::Matrix<double,8,1> ret;
  Eigen::Vector3d centroid=_act->_objects.find(Object(_obj))->_features.find(frm)->second._centroid;
  ret[0]=(centroid-_act->_skeletons.find(frm)->_joints[HEAD_JOINT_NUM].translation()).norm();
  ret[1]=(centroid-_act->_skeletons.find(frm)->_joints[NECK_JOINT_NUM].translation()).norm();
  ret[2]=(centroid-_act->_skeletons.find(frm)->_joints[LEFT_SHOULDER_JOINT_NUM].translation()).norm();
  ret[3]=(centroid-_act->_skeletons.find(frm)->_joints[LEFT_ELBOW_JOINT_NUM].translation()).norm();
  ret[4]=(centroid-_act->_skeletons.find(frm)->_joints[RIGHT_SHOULDER_JOINT_NUM].translation()).norm();
  ret[5]=(centroid-_act->_skeletons.find(frm)->_joints[RIGHT_ELBOW_JOINT_NUM].translation()).norm();
  ret[6]=(centroid-_act->_skeletons.find(frm)->_poses[POS_LEFT_HAND_NUM]).norm();
  ret[7]=(centroid-_act->_skeletons.find(frm)->_poses[POS_RIGHT_HAND_NUM]).norm();
  return ret;
}
int CRF::OATerm::nrFeats()
{
  return 40;
}
//OOTTerm: consistent with paper
CRF::OOTTerm::OOTTerm(const Activity* act,const EdgeWeight& w,const SubActivity* segment1,const SubActivity* segment2,int obj)
  :Term(act),_w(w),_segment1(segment1),_segment2(segment2),_obj(obj) {}
pair<Eigen::VectorXd,int> CRF::OOTTerm::calcFeature() const
{
  pair<Eigen::VectorXd,int> ret=_w.find(_segment1->_affordance[_obj])->second.find(_segment2->_affordance[_obj])->second;
  int end=max(_segment1->_end,_segment2->_end);
  int start=min(_segment1->_start,_segment2->_start);
  vector<Eigen::Vector3d> centroid(end-start+1);
  for(int i=start; i<=end; i++)
    centroid[i-start]=_act->_objects.find(Object(_obj))->_features.find(i)->second._centroid;
  ret.first.setZero();
  for(int i=start+1; i<=end; i++) {
    ret.first[0]+=abs(centroid[i-start][2]-centroid[i-start-1][2]);
    ret.first[2]+=(centroid[i-start]-centroid[i-start-1]).norm();
  }
  ret.first[1]=ret.first[0]/(double)max<int>(end-start,1);
  ret.first[3]=ret.first[2]/(double)max<int>(end-start,1);
  ASSERT(ret.first[1] <= ret.first[0])
  ASSERT(ret.first[3] <= ret.first[2])
  return ret;
}
void CRF::OOTTerm::buildLPProb(LPProb& lp) const
{
  Eigen::VectorXd f=calcFeature().first;
  int Y1=_segment1->_LPIndexAffordance[_obj];
  int Y2=_segment2->_LPIndexAffordance[_obj];
  LP::buildLPProbCRF(lp,_w,f,Y1,Y2);
}
int CRF::OOTTerm::nrFeats()
{
  return 4;
}
//AATTerm: consistent with paper
CRF::AATTerm::AATTerm(const Activity* act,const EdgeWeight& w,const SubActivity* segment1,const SubActivity* segment2)
  :Term(act),_w(w),_segment1(segment1),_segment2(segment2) {}
pair<Eigen::VectorXd,int> CRF::AATTerm::calcFeature() const
{
  pair<Eigen::VectorXd,int> ret=_w.find(_segment1->_type)->second.find(_segment2->_type)->second;
  int end=max(_segment1->_end,_segment2->_end);
  int start=min(_segment1->_start,_segment2->_start);
  vector<vector<Eigen::Vector3d> > jointPos(8,vector<Eigen::Vector3d>(end-start+1));
  for(int i=start; i<=end; i++) {
    jointPos[0][i-start]=_act->_skeletons.find(i)->_joints[HEAD_JOINT_NUM].translation();
    jointPos[1][i-start]=_act->_skeletons.find(i)->_joints[NECK_JOINT_NUM].translation();
    jointPos[2][i-start]=_act->_skeletons.find(i)->_joints[LEFT_SHOULDER_JOINT_NUM].translation();
    jointPos[3][i-start]=_act->_skeletons.find(i)->_joints[LEFT_ELBOW_JOINT_NUM].translation();
    jointPos[4][i-start]=_act->_skeletons.find(i)->_joints[RIGHT_SHOULDER_JOINT_NUM].translation();
    jointPos[5][i-start]=_act->_skeletons.find(i)->_joints[RIGHT_ELBOW_JOINT_NUM].translation();
    jointPos[6][i-start]=_act->_skeletons.find(i)->_poses[POS_LEFT_HAND_NUM];
    jointPos[7][i-start]=_act->_skeletons.find(i)->_poses[POS_RIGHT_HAND_NUM];
  }
  ret.first.setZero();
  for(int j=0; j<8; j++) {
    for(int i=start+1; i<=end; i++)
      ret.first[j]+=(jointPos[j][i-start]-jointPos[j][i-start-1]).norm();
    ret.first[j+8]=ret.first[j]/(double)max<int>(end-start,1);
    ASSERT(ret.first[j+8] <= ret.first[j])
  }
  return ret;
}
void CRF::AATTerm::buildLPProb(LPProb& lp) const
{
  Eigen::VectorXd f=calcFeature().first;
  int Y1=_segment1->_LPIndexType;
  int Y2=_segment2->_LPIndexType;
  LP::buildLPProbCRF(lp,_w,f,Y1,Y2);
}
int CRF::AATTerm::nrFeats()
{
  return 16;
}
//OTerm: consistent with paper
CRF::OTerm::OTerm(const Activity* act,const VertexWeight& w,const SubActivity* segment,int obj)
  :Term(act),_w(w),_segment(segment),_obj(obj) {}
pair<Eigen::VectorXd,int> CRF::OTerm::calcFeature() const
{
  pair<Eigen::VectorXd,int> ret=_w.find(_segment->_affordance[_obj])->second;
  int start=_segment->_start;
  int end=_segment->_end;
  ret.first.setZero();
  vector<Eigen::Matrix<double,13,1> > feats(end-start+1);
  for(int i=start; i<=end; i++) {
    feats[i-start]=calcFeature(i);
    //centroid, bounding box
    ret.first.segment<13>(0)+=feats[i-start];
    if(i > start)
      //displacement
      ret.first[14]+=(feats[i-start]-feats[i-start-1]).segment<3>(0).norm();
  }
  //normalize feature
  ret.first.segment<13>(0)/=(double)feats.size();
  //distance
  ret.first[13]=(feats.back()-feats.front()).segment<3>(0).norm();
  ASSERT(ret.first[14] >= ret.first[13])
  return ret;
}
void CRF::OTerm::buildLPIndex(LPProb& lp,map<int,string>& nameBinding)
{
  int index=(int)lp._coef.size();
  SubActivity& s=const_cast<SubActivity&>(*_segment);
  s._LPIndexAffordance.resize(s._affordance.size());
  s._LPIndexAffordance[_obj]=index;
  ITERV(_w,const_iterator) {
    lp._coef.push_back(LPCoef());
    lp._Y.push_back(s._affordance[_obj] == beg->first ? 1 : 0);
    nameBinding[index++]=boost::lexical_cast<string>(_segment->_start)+":"+boost::lexical_cast<string>(_obj)+":"+beg->first;
  }
}
void CRF::OTerm::buildLPProb(LPProb& lp) const
{
  Eigen::VectorXd f=calcFeature().first;
  int index=_segment->_LPIndexAffordance[_obj];
  LP::buildLPProbCRF(lp,_w,f,index);
}
void CRF::OTerm::assignLPResult(const Eigen::VectorXd& result)
{
  SubActivity& s=const_cast<SubActivity&>(*_segment);
  int index=s._LPIndexAffordance[_obj];
  double resultMax = result[index];
  s._affordance[_obj]=_w.begin()->first;
  ITERV(_w,const_iterator) {
    if(result[index] > resultMax) {
      resultMax = result[index];
      s._affordance[_obj]=beg->first;
    }
    index++;
  }
}
Eigen::Matrix<double,13,1> CRF::OTerm::calcFeature(int frm) const
{
  int off=0;
  const ObjectFeature& feat=_act->_objects.find(Object(_obj))->_features.find(frm)->second;
  Eigen::Matrix<double,13,1> ret;
  ret.segment<3>(off)=feat._centroid;
  off+=3;
  ret.segment<10>(off)=feat._feature.segment<10>(0);
  return ret;
}
int CRF::OTerm::nrFeats()
{
  return 15;
}
//ATerm: consistent with paper
bool isInf(const double& x)
{
  return ( ( x - x ) != double(0.0f) );
}
bool isNan(const double& x)
{
  return ( ( ! ( x < 0.0f ) ) && ( ! ( x >= 0.0f ) ) );
}
Eigen::Vector4d toQuatSafe(const Eigen::Matrix3d& rot)
{
  Eigen::Quaterniond quat(rot);
  if(!isInf(quat.x()) && !isInf(quat.y()) && !isInf(quat.z()) && !isInf(quat.w()) &&
     !isNan(quat.x()) && !isNan(quat.y()) && !isNan(quat.z()) && !isNan(quat.w()))
    return Eigen::Vector4d(quat.x(),quat.y(),quat.z(),quat.w());
  else return Eigen::Vector4d::Zero();
}
double angleBetween(const Eigen::Vector3d& a,const Eigen::Vector3d& b)
{
  double dotVal=a.dot(b)/max<double>(a.norm(),1E-10f)/max<double>(b.norm(),1E-10f);
  return acos(dotVal);
}
CRF::ATerm::ATerm(const Activity* act,const VertexWeight& w,const SubActivity* segment)
  :Term(act),_w(w),_segment(segment) {}
pair<Eigen::VectorXd,int> CRF::ATerm::calcFeature() const
{
  pair<Eigen::VectorXd,int> ret=_w.find(_segment->_type)->second;
  int start=_segment->_start;
  int end=_segment->_end;
  ret.first.setZero();
  int denom=0;
  vector<Eigen::Vector2d> handVerticalPos;
  vector<Eigen::Matrix<double,24,1> > feats(end-start+1);
  for(int i=start; i<=end; i++) {
    feats[i-start]=calcFeature(i);
    //mean position
    ret.first.segment<24>(0)+=feats[i-start];
    if(i > start)
      //displacement
      for(int j=0; j<8; j++)
        ret.first[24+8+j]+=(feats[i-start].segment<3>(j*3)-feats[i-start-1].segment<3>(j*3)).norm();
    //pose feature
    ret.first.segment<47>(24+16)+=calcPoseFeature(i);
    //hand feature
    ret.first.segment<16>(24+16+47)+=calcHandFeature(i,handVerticalPos,denom);
  }
  //normalize joint position
  ret.first.segment<24>(0)/=(double)feats.size();
  //distance
  for(int j=0; j<8; j++) {
    ret.first[24+j]=(feats.back().segment<3>(j*3)-feats.front().segment<3>(j*3)).norm();
    ASSERT(ret.first[24+8+j] >= ret.first[24+j])
  }
  //normalize pose feature
  ret.first.segment<47>(24+16)/=(double)feats.size();
  //normalize hand feature
  ret.first.segment<16>(24+16+47)/=(double)max<int>(denom,1);
  return ret;
}
void CRF::ATerm::buildLPIndex(LPProb& lp,map<int,string>& nameBinding)
{
  int index=(int)lp._coef.size();
  SubActivity& s=const_cast<SubActivity&>(*_segment);
  s._LPIndexType=index;
  ITERV(_w,const_iterator) {
    lp._coef.push_back(LPCoef());
    lp._Y.push_back(s._type == beg->first ? 1 : 0);
    nameBinding[index++]=boost::lexical_cast<string>(_segment->_start)+":"+beg->first;
  }
}
void CRF::ATerm::buildLPProb(LPProb& lp) const
{
  Eigen::VectorXd f=calcFeature().first;
  int index=_segment->_LPIndexType;
  LP::buildLPProbCRF(lp,_w,f,index);
}
void CRF::ATerm::assignLPResult(const Eigen::VectorXd& result)
{
  SubActivity& s=const_cast<SubActivity&>(*_segment);
  int index=s._LPIndexType;
  double resultMax=result[index];
  s._type = _w.begin()->first;
  ITERV(_w,const_iterator) {
    if(result[index] > resultMax) {
      resultMax = result[index];
      s._type=beg->first;
    }
    index++;
  }
}
Eigen::Matrix<double,24,1> CRF::ATerm::calcFeature(int frm) const
{
  int off=0;
  Eigen::Matrix<double,24,1> ret;
  ret.segment<3>(off)=_act->_skeletons.find(frm)->_joints[HEAD_JOINT_NUM].translation();
  off+=3;
  ret.segment<3>(off)=_act->_skeletons.find(frm)->_joints[NECK_JOINT_NUM].translation();
  off+=3;
  ret.segment<3>(off)=_act->_skeletons.find(frm)->_joints[LEFT_SHOULDER_JOINT_NUM].translation();
  off+=3;
  ret.segment<3>(off)=_act->_skeletons.find(frm)->_joints[LEFT_ELBOW_JOINT_NUM].translation();
  off+=3;
  ret.segment<3>(off)=_act->_skeletons.find(frm)->_joints[RIGHT_SHOULDER_JOINT_NUM].translation();
  off+=3;
  ret.segment<3>(off)=_act->_skeletons.find(frm)->_joints[RIGHT_ELBOW_JOINT_NUM].translation();
  off+=3;
  ret.segment<3>(off)=_act->_skeletons.find(frm)->_poses[POS_LEFT_HAND_NUM];
  off+=3;
  ret.segment<3>(off)=_act->_skeletons.find(frm)->_poses[POS_RIGHT_HAND_NUM];
  return ret;
}
Eigen::Matrix<double,47,1> CRF::ATerm::calcPoseFeature(int frm) const
{
  int off=0;
  Eigen::Matrix<double,47,1> ret;
  Eigen::Matrix3d invTorso=_act->_skeletons.find(frm)->_joints[TORSO_JOINT_NUM].linear().inverse();
  Eigen::Vector3d posTorso=_act->_skeletons.find(frm)->_joints[TORSO_JOINT_NUM].translation();
  ret.segment<4>(off)=toQuatSafe(invTorso*_act->_skeletons.find(frm)->_joints[HEAD_JOINT_NUM].linear());
  off+=4;
  ret.segment<4>(off)=toQuatSafe(invTorso*_act->_skeletons.find(frm)->_joints[NECK_JOINT_NUM].linear());
  off+=4;
  ret.segment<4>(off)=toQuatSafe(invTorso*_act->_skeletons.find(frm)->_joints[LEFT_SHOULDER_JOINT_NUM].linear());
  off+=4;
  ret.segment<4>(off)=toQuatSafe(invTorso*_act->_skeletons.find(frm)->_joints[LEFT_ELBOW_JOINT_NUM].linear());
  off+=4;
  ret.segment<4>(off)=toQuatSafe(invTorso*_act->_skeletons.find(frm)->_joints[RIGHT_SHOULDER_JOINT_NUM].linear());
  off+=4;
  ret.segment<4>(off)=toQuatSafe(invTorso*_act->_skeletons.find(frm)->_joints[RIGHT_ELBOW_JOINT_NUM].linear());
  off+=4;
  ret.segment<4>(off)=toQuatSafe(invTorso*_act->_skeletons.find(frm)->_joints[LEFT_HIP_JOINT_NUM].linear());
  off+=4;
  ret.segment<4>(off)=toQuatSafe(invTorso*_act->_skeletons.find(frm)->_joints[LEFT_KNEE_JOINT_NUM].linear());
  off+=4;
  ret.segment<4>(off)=toQuatSafe(invTorso*_act->_skeletons.find(frm)->_joints[RIGHT_HIP_JOINT_NUM].linear());
  off+=4;
  ret.segment<4>(off)=toQuatSafe(invTorso*_act->_skeletons.find(frm)->_joints[RIGHT_KNEE_JOINT_NUM].linear());
  off+=4;
  ret.segment<3>(off)=_act->_skeletons.find(frm)->_poses[POS_LEFT_FOOT_NUM]-posTorso;
  off+=3;
  ret.segment<3>(off)=_act->_skeletons.find(frm)->_poses[POS_RIGHT_FOOT_NUM]-posTorso;
  off+=3;
  ret[off]=angleBetween(_act->_skeletons.find(frm)->_joints[HEAD_JOINT_NUM].translation()-
                        (_act->_skeletons.find(frm)->_joints[LEFT_HIP_JOINT_NUM].translation()+
                         _act->_skeletons.find(frm)->_joints[RIGHT_HIP_JOINT_NUM].translation())/2,
                        Eigen::Vector3d::Unit(2));
  return ret;
}
Eigen::Matrix<double,16,1> CRF::ATerm::calcHandFeature(int frm,vector<Eigen::Vector2d>& handVerticalPos,int& denom) const
{
  Eigen::Matrix<double,16,1> ret;
  handVerticalPos.push_back(Eigen::Vector2d());
  handVerticalPos.back()[0]=_act->_skeletons.find(frm)->_poses[POS_LEFT_HAND_NUM][2];
  handVerticalPos.back()[1]=_act->_skeletons.find(frm)->_poses[POS_RIGHT_HAND_NUM][2];
  if(handVerticalPos.size() > 6)
    handVerticalPos.erase(handVerticalPos.begin());

  ret.setZero();
  if(handVerticalPos.size() == 6) {
    int off=0;
    ret.segment<2>(off)=handVerticalPos[0];
    off+=2;
    ret.segment<2>(off)=handVerticalPos[1];
    off+=2;
    ret.segment<2>(off)=handVerticalPos[2];
    off+=2;
    ret.segment<2>(off)=handVerticalPos[3];
    off+=2;
    ret.segment<2>(off)=handVerticalPos[4];
    off+=2;
    ret.segment<2>(off)=handVerticalPos[5];
    off+=2;
    ret.segment<2>(off)=handVerticalPos[0].cwiseMax(handVerticalPos[1]).cwiseMax(handVerticalPos[2]).cwiseMax(handVerticalPos[3]).cwiseMax(handVerticalPos[4]).cwiseMax(handVerticalPos[5]);
    off+=2;
    ret.segment<2>(off)=handVerticalPos[0].cwiseMin(handVerticalPos[1]).cwiseMin(handVerticalPos[2]).cwiseMin(handVerticalPos[3]).cwiseMin(handVerticalPos[4]).cwiseMin(handVerticalPos[5]);
    ASSERT(ret[12] >= ret[0] && ret[12] >= ret[2] && ret[12] >= ret[4] && ret[12] >= ret[6] && ret[12] >= ret[8] && ret[12] >= ret[10])
    ASSERT(ret[13] >= ret[1] && ret[13] >= ret[3] && ret[13] >= ret[5] && ret[13] >= ret[7] && ret[13] >= ret[9] && ret[13] >= ret[11])
    ASSERT(ret[14] <= ret[0] && ret[14] <= ret[2] && ret[14] <= ret[4] && ret[14] <= ret[6] && ret[14] <= ret[8] && ret[14] <= ret[10])
    ASSERT(ret[15] <= ret[1] && ret[15] <= ret[3] && ret[15] <= ret[5] && ret[15] <= ret[7] && ret[15] <= ret[9] && ret[15] <= ret[11])
    denom++;
  }
  return ret;
}
int CRF::ATerm::nrFeats()
{
  return 103;
}
//CRF
void CRF::setZero()
{
  ZEROV(_O)
  ZEROV(_A)
  ZEROE(_OO)
  ZEROE(_OA)
  ZEROE(_OOT)
  ZEROE(_AAT)
}
void CRF::setRandom()
{
  RANDOMV(_O)
  RANDOMV(_A)
  RANDOME(_OO)
  RANDOME(_OA)
  RANDOME(_OOT)
  RANDOME(_AAT)
}
void CRF::assign(const Eigen::VectorXd& ret)
{
  ASSERT(ret.size() == _nrW);
  ASSIGNV(_O)
  ASSIGNV(_A)
  ASSIGNE(_OO)
  ASSIGNE(_OA)
  ASSIGNE(_OOT)
  ASSIGNE(_AAT)
}
Eigen::VectorXd CRF::assemble() const
{
  Eigen::VectorXd ret;
  ret.setZero(_nrW);
  ASSEMBLEV(_O)
  ASSEMBLEV(_A)
  ASSEMBLEE(_OO)
  ASSEMBLEE(_OA)
  ASSEMBLEE(_OOT)
  ASSEMBLEE(_AAT)
  return ret;
}
void CRF::init(const Activities& acts)
{
#define ADDFEATSV(C,NRF,W)	\
for(map<string,string>::const_iterator beg=C.begin(),end=C.end();beg!=end;beg++,_nrW+=NRF)	\
W[beg->second]=make_pair(Eigen::VectorXd::Zero(NRF),_nrW);
#define ADDFEATSE(C1,C2,NRF,W)	\
for(map<string,string>::const_iterator beg=C1.begin(),end=C1.end();beg!=end;beg++)	\
for(map<string,string>::const_iterator beg2=C2.begin(),end2=C2.end();beg2!=end2;beg2++,_nrW+=NRF)	\
	W[beg->second][beg2->second]=make_pair(Eigen::VectorXd::Zero(NRF),_nrW);
  _nrW=0;
  ADDFEATSV(acts._affMap,OTerm::nrFeats(),_O)
  ADDFEATSV(acts._classMap,ATerm::nrFeats(),_A)
  ADDFEATSE(acts._affMap,acts._affMap,OOTerm::nrFeats(),_OO)
  ADDFEATSE(acts._affMap,acts._classMap,OATerm::nrFeats(),_OA)
  ADDFEATSE(acts._affMap,acts._affMap,OOTTerm::nrFeats(),_OOT)
  ADDFEATSE(acts._classMap,acts._classMap,AATTerm::nrFeats(),_AAT)
#undef ADDFEATSV
#undef ADDFEATSE
  for(set<Activity>::const_iterator beg=acts._activities.begin(),end=acts._activities.end(); beg!=end; beg++)
    addActivity(*beg,_terms);

#define DEBUG_CRF
#ifdef DEBUG_CRF
  //DEBUG CODE: test compute feature
  #pragma omp parallel for
  for(int i=0; i<(int)_terms.size(); i++)
    _terms[i]->calcFeature();
  //DEBUG CODE: test data-structure consistency
  //set random
  Eigen::VectorXd tmp=assemble();
  tmp.setRandom(tmp.size());
  assign(tmp);
  //check consistency
  tmp=assemble();
  VertexWeight O=_O,A=_A;
  EdgeWeight OO=_OO,OA=_OA,OOT=_OOT,AAT=_AAT;
  setZero();
  assign(tmp);
  ASSERT(O == _O && A == _A && OO == _OO && OA == _OA && OOT == _OOT && AAT == _AAT)
#endif
}
//prediction code
void CRF::predicate(const Activity& act)
{
  //add terms
  vector<boost::shared_ptr<Term> > terms;
  addActivity(act,terms);

  //build terms
  LPProb lp;
  for(int i=0; i<(int)terms.size(); i++)
    terms[i]->buildLPIndex(lp,lp._nameBinding);
  for(int i=0; i<(int)terms.size(); i++)
    terms[i]->buildLPProb(lp);

  //solve LP
  //variable type is now continuous
  lp._varType=LPProb::CONTINUOUS01;
  boost::filesystem::ofstream os("./LPProb.txt");
  LP::solveGLPK(lp,&os);
  //result is clamped when assigning
  for(int i=0; i<(int)terms.size(); i++)
    terms[i]->assignLPResult(lp._result);
}
void CRF::read(const string& path)
{
  boost::filesystem::ifstream is(path,ios::binary);
  boost::archive::binary_iarchive ar(is);
  ar & *this;
}
void CRF::write(const string& path)
{
  boost::filesystem::ofstream os(path,ios::binary);
  boost::archive::binary_oarchive ar(os);
  ar & *this;
}
void CRF::addActivity(const Activity& act,vector<boost::shared_ptr<Term> >& terms)
{
  //build CRF
  for(set<SubActivity>::const_iterator beg=act._subActivities.begin(),end=act._subActivities.end(),last=beg; beg!=end; last=beg,beg++) {
    terms.push_back(boost::shared_ptr<Term>(new ATerm(&act,_A,&*beg)));
    if(beg != last)terms.push_back(boost::shared_ptr<Term>(new AATTerm(&act,_AAT,&*beg,&*last)));
    for(int i=1; i<=(int)act._objects.size(); i++) {
      terms.push_back(boost::shared_ptr<Term>(new OTerm(&act,_O,&*beg,i)));
      terms.push_back(boost::shared_ptr<Term>(new OATerm(&act,_OA,&*beg,i)));
      if(beg != last)terms.push_back(boost::shared_ptr<Term>(new OOTTerm(&act,_OOT,&*beg,&*last,i)));
      for(int j=i+1; j<=(int)act._objects.size(); j++)
        terms.push_back(boost::shared_ptr<Term>(new OOTerm(&act,_OO,&*beg,i,j)));
    }
  }
}
//Linear Programming Interface
void LP::solveGraphCutQPBO(LPProb& lp)
{
  //solver setup
  int index=0;
  for(map<int,map<int,LPCoef> >::const_iterator beg2=lp._quadraticTerms.begin(),end2=lp._quadraticTerms.end(); beg2!=end2; beg2++)
    for(map<int,LPCoef>::const_iterator beg=beg2->second.begin(),end=beg2->second.end(); beg!=end; beg++)
      index++;
  QPBO<double>* q=new QPBO<double>(lp._coef.size()+lp._Y.size(),index);
  //add vertices: we negate everything to transform minimization to maximization
  q->AddNode(lp._coef.size());
  for(int i=0; i<(int)lp._coef.size(); i++)
    q->AddUnaryTerm(i,0,-lp._coef[i]._coef);
  for(int i=0; i<(int)lp._Y.size(); i++)
    if(lp._Y[i] == 0)
      q->AddUnaryTerm(i,0,-1);
    else q->AddUnaryTerm(i,-1,0);
  //add edges: we negate everything to transform minimization to maximization
#define E(x,y,Y) (x-Y)*(y-Y)
  for(map<int,map<int,LPCoef> >::const_iterator beg2=lp._quadraticTerms.begin(),end2=lp._quadraticTerms.end(); beg2!=end2; beg2++)
    for(map<int,LPCoef>::const_iterator beg=beg2->second.begin(),end=beg2->second.end(); beg!=end; beg++)
      q->AddPairwiseTerm(beg2->first,beg->first,0,0,0,-beg->second._coef);
  //solve
  q->Solve();
  q->ComputeWeakPersistencies();
  lp._result.setZero(lp._coef.size());
  for(int i=0; i<(int)lp._coef.size(); i++) {
    lp._result[i]=q->GetLabel(i);
    if(lp._result[i] < 0)
      lp._result[i]=0.5f;
  }
  delete q;
}
void LP::solveGraphCut(LPProb& lp,ostream* os)
{
  //if the problem can be solved by graph cut
  //then no constraints are allowed
  lp._trips.clear();
  lp._rhs.clear();
  lp._type.clear();
  //and problem must be relaxed
  //all variable fall in [0,1]
  int nrC=(int)lp._coef.size();
  relaxQP(lp);
  //solve the problem, the solution must fall in [0,0.5,1]
  lp._varType=LPProb::CONTINUOUS01;
  solveGLPK(lp,os);
  lp._result=lp._result.block(0,0,nrC,1).eval();
}
void LP::solveGLPK(LPProb& lp,ostream* os)
{
  relaxQP(lp);
  //create problem
  glp_prob* glp=glp_create_prob();
  glp_set_prob_name(glp,"predicate");
  glp_set_obj_dir(glp,GLP_MAX);
  //fill problem variables and objective
  if(os)
    *os << lp._coef.size() << " " <<
        ((lp._varType == LPProb::BINARY) ? "binary" :
        (lp._varType == LPProb::CONTINUOUS01) ? "continuous01" :
        "continuous") << " variable definitions:" << endl;
  glp_add_cols(glp,lp._coef.size());
  for(int i=1; i<=(int)lp._coef.size(); i++) {
    //variable objective
    glp_set_obj_coef(glp,i,lp._coef[i-1]._coef);
    //variable type
    if(lp._varType == LPProb::BINARY)
      glp_set_col_kind(glp,i,GLP_IV);
    else glp_set_col_kind(glp,i,GLP_CV);
    //variable bound
    if(lp._varType == LPProb::CONTINUOUS)
      glp_set_col_bnds(glp,i,GLP_FR,0,0);
    else glp_set_col_bnds(glp,i,GLP_DB,0,1);
    if(os)
      *os << "variable " << lp._nameBinding.find(i-1)->second << "=" << lp._coef[i-1]._coef << endl;
  }
  //fill problem constraints
  if(os) {
    Eigen::SparseMatrix<double,Eigen::RowMajor> lhs;
    lhs.resize(lp._rhs.size(),lp._coef.size());
    lhs.setFromTriplets(lp._trips.begin(),lp._trips.end());
    *os << lp._rhs.size() << " linear constraints:" << endl;
    for(int k=0; k<lhs.outerSize(); k++) {
      bool first=true;
      for(Eigen::SparseMatrix<double,Eigen::RowMajor>::InnerIterator it(lhs,k); it; ++it) {
        if(first && it.value() > 0)
          *os << "";
        else *os << (it.value() >= 0 ? "+" : "-");
        if(abs(it.value()) != 1)
          *os << abs(it.value()) << "*";
        *os << lp._nameBinding.find(it.col())->second;
        first=false;
      }
      *os << (lp._type[k] == 0 ? "=" : lp._type[k] < 0 ? "<=" : ">=") << lp._rhs[k] << endl;
    }
  }
  if(lp._rhs.size() > 0)
    glp_add_rows(glp,lp._rhs.size());
  for(int i=1; i<=(int)lp._rhs.size(); i++)
    glp_set_row_bnds(glp,i,lp._type[i-1] < 0 ? GLP_UP : lp._type[i-1] > 0 ? GLP_LO : GLP_FX,lp._rhs[i-1],lp._rhs[i-1]);
  vector<int> ia(lp._trips.size()+1),ja(lp._trips.size()+1);
  vector<double> ar(lp._trips.size()+1);
  #pragma omp parallel
  for(int i=1; i<=(int)lp._trips.size(); i++) {
    ia[i]=lp._trips[i-1].row()+1;
    ja[i]=lp._trips[i-1].col()+1;
    ar[i]=lp._trips[i-1].value();
  }
  glp_load_matrix(glp,(int)lp._trips.size(),&(ia[0]),&(ja[0]),&(ar[0]));
  //solve problem
  double startTime = get_wall_time();
  glp_simplex(glp,NULL);
  if(lp._varType == LPProb::BINARY)
    glp_intopt(glp,NULL);
  //memory usage
  std::size_t total;
  glp_mem_usage(NULL, NULL, &total, NULL);
  cout << "Time elapsed: " << get_wall_time() - startTime << "sec, "
       << "Memory usage: " << total << " bytes" << endl;

  //recover and display results
  lp._result.resize(lp._coef.size());
  if(os)
    *os << "optimal value=" << glp_get_obj_val(glp) << " solution:" << endl;
  for(int i=1; i<=(int)lp._coef.size(); i++) {
    if(lp._varType == LPProb::BINARY)
      lp._result[i-1]=glp_mip_col_val(glp,i);
    else lp._result[i-1]=glp_get_col_prim(glp,i);
    if(os)
      *os << lp._nameBinding.find(i-1)->second << "=" << lp._result[i-1] << endl;
  }
  //housekeeping
  glp_delete_prob(glp);
  glp_free_env();
}
void LP::relaxQP(LPProb& lp)
{
  int index=(int)lp._coef.size();
  for(map<int,map<int,LPCoef> >::const_iterator beg2=lp._quadraticTerms.begin(),end2=lp._quadraticTerms.end(); beg2!=end2; beg2++)
    for(map<int,LPCoef>::const_iterator beg=beg2->second.begin(),end=beg2->second.end(); beg!=end; beg++) {
      //new variable and objective
      lp._coef.push_back(beg->second);
      //S2-relaxation
      int Y1=beg2->first;
      int Y2=beg->first;
      //constraints 1 for each z
      lp._trips.push_back(Eigen::Triplet<double,int>(lp._rhs.size(),index,1));
      lp._trips.push_back(Eigen::Triplet<double,int>(lp._rhs.size(),Y1,-1));
      lp._rhs.push_back(0);
      lp._type.push_back(-1);
      //constraints 2 for each z
      lp._trips.push_back(Eigen::Triplet<double,int>(lp._rhs.size(),index,1));
      lp._trips.push_back(Eigen::Triplet<double,int>(lp._rhs.size(),Y2,-1));
      lp._rhs.push_back(0);
      lp._type.push_back(-1);
      //constraints 3 for each z
      lp._trips.push_back(Eigen::Triplet<double,int>(lp._rhs.size(),index,-1));
      lp._trips.push_back(Eigen::Triplet<double,int>(lp._rhs.size(),Y1,1));
      lp._trips.push_back(Eigen::Triplet<double,int>(lp._rhs.size(),Y2,1));
      lp._rhs.push_back(1);
      lp._type.push_back(-1);
      //insert new name
      (lp._nameBinding)[index]=(lp._nameBinding)[beg2->first]+"_"+(lp._nameBinding)[beg->first];
      index++;
    }
  lp._quadraticTerms.clear();
}
void LP::buildLPProbCRF(LPProb& lp,const CRF::VertexWeight& w,const Eigen::VectorXd& f,int index)
{
  for(CRF::VertexWeight::const_iterator beg=w.begin(),end=w.end(); beg!=end; beg++) {
    //objective term
    lp._coef[index]+=LPCoef(f.dot(beg->second.first),beg->second.second,f);
    //and must sum to one
    lp._trips.push_back(Eigen::Triplet<double,int>(lp._rhs.size(),index,1));
    index++;
  }
  lp._rhs.push_back(1);
  lp._type.push_back(0);
}
void LP::buildLPProbCRF(LPProb& lp,const CRF::EdgeWeight& w,const Eigen::VectorXd& f,int indexY1,int indexY2)
{
  int Y1=indexY1;
  int Y2=indexY2;
  for(CRF::EdgeWeight::const_iterator beg2=w.begin(),end2=w.end(); beg2!=end2; beg2++,Y1++,Y2=indexY2)
    for(CRF::VertexWeight::const_iterator beg=beg2->second.begin(),end=beg2->second.end(); beg!=end; beg++,Y2++)
      lp._quadraticTerms[Y1][Y2]+=LPCoef(f.dot(beg->second.first),beg->second.second,f);
}
void LP::runExample()
{
#define N 20
  LPProb lp;
  //creat an image
  Eigen::MatrixXi image;
  image.resize(N,N);
  for(int i=0,index=0; i<N; i++)
    for(int j=0; j<N; j++)
      image(i,j)=index++;
  //build random MRF
  lp._coef.resize(N*N);
  for(int i=0; i<N; i++)
    for(int j=0; j<N; j++) {
      for(int k=-2; k<0; k++)
        for(int l=-2; l<0; l++)
          if(i+k >= 0 && j+l >= 0)
            lp._quadraticTerms[image(i,j)][image(i+k,j+l)]=LPCoef(rand()/(double)RAND_MAX*2-1);
      lp._coef[image(i,j)]=LPCoef(rand()/(double)RAND_MAX*2-1);
    }
  //solve using both solvers
  LPProb lp2=lp;
  solveGraphCutQPBO(lp);
  solveGraphCut(lp2,NULL);
  cout << "GLP-QPBO error: " << (lp._result-lp2._result).squaredNorm() << endl;
  for(int i=0; i<N; i++) {
    for(int j=0; j<N; j++)
      cout << lp._result[image(i,j)] << " ";
    cout << endl;
  }
  //now add the loss function
  LPProb lp3=lp;
  lp3._Y.resize(lp3._coef.size());
  for(int i=0; i<N; i++)
    for(int j=0; j<N; j++)
      lp3._Y[image(i,j)]=rand()/(double)RAND_MAX > 0.5 ? 1 : 0;
  solveGraphCutQPBO(lp3);
  cout << "loss-noloss difference: " << (lp._result-lp3._result).squaredNorm() << endl;
  for(int i=0; i<N; i++) {
    for(int j=0; j<N; j++)
      cout << lp3._result[image(i,j)] << " ";
    cout << endl;
  }
  //now check if we really achieved the best solution in half-integer space
  //given the LPEval(Y*) >= LPEval(Y) \forall Y in {0,0.5,1}
  Eigen::VectorXd anotherY=lp3._result;
  double valOpt=lp3.eval(lp3._result);
  for(int i=0;i<(int)anotherY.size();i++) {
    anotherY[i]=0;
    ASSERT(lp.eval(anotherY) <= valOpt)
    anotherY[i]=0.5;
    ASSERT(lp.eval(anotherY) <= valOpt)
    anotherY[i]=1;
    ASSERT(lp.eval(anotherY) <= valOpt)
    anotherY[i]=lp3._result[i];
  }
#undef N
}
//quadratic programming interface
void grad(const alglib::real_1d_array& x,double& func,alglib::real_1d_array& grad,void* ptr)
{
  const QPProb& qp=*((QPProb*)ptr);
  Eigen::VectorXd xEigen=Eigen::Map<const Eigen::VectorXd>(x.getcontent(),x.length());
  Eigen::Map<Eigen::VectorXd>(grad.getcontent(),grad.length())=qp._A*xEigen+qp._b;
  func=xEigen.dot(qp._A*xEigen/2+qp._b);
}
void QP::solveQPPrimal(QPProb& qp)
{
  qp._result.setZero(qp._A.rows());
  alglib::real_1d_array x;
  x.setcontent(qp._result.size(),qp._result.data());
  alglib::integer_1d_array ct;
  alglib::real_2d_array c;
  c.setlength(qp._rhs.size(),qp._A.rows()+1);
  ct.setlength(qp._rhs.size());
  for(int i=0;i<(int)qp._rhs.size();i++) {
    ct[i]=1;
    for(int j=0;j<(int)qp._A.rows();j++)
      c(i,j)=qp._cons[i].coeff(j);
    c(i,qp._A.rows())=qp._rhs[i];
  }
  alglib::minbleicreport rep;
  alglib::minbleicstate state;
  alglib::minbleiccreate(x,state);
  if(qp._rhs.size() > 0)
    alglib::minbleicsetlc(state,c,ct);
  alglib::minbleicsetcond(state,0,0,0,100000000);
  alglib::minbleicoptimize(state,grad,NULL,&qp);
  alglib::minbleicresults(state,x,rep);
  //ASSERT(rep.terminationtype <= 4)
  qp._result=Eigen::Map<const Eigen::VectorXd>(x.getcontent(),x.length());
}
void QP::solveQPDual(QPProb& qp)
{
  //form dual
  Eigen::DiagonalMatrix<double,-1> invA(qp._A.rows());
  for(int i=0;i<invA.rows();i++) {
    ASSERT(qp._A.diagonal()[i] >= 0)
    if(qp._A.diagonal()[i] > 0)
      invA.diagonal()[i]=1/qp._A.diagonal()[i];
  }
  Eigen::VectorXd invAb=invA*qp._b;

  int n=(int)qp._cons.size();
  alglib::real_2d_array hx;
  alglib::real_1d_array b,x,bndl,bndu;
  hx.setlength(n,n);
  b.setlength(n);
  bndl.setlength(n);
  bndu.setlength(n);
  for(int i=0;i<n;i++) {
    b[i]=-qp._cons[i].dot(invAb)-qp._rhs[i];
    bndl[i]=0;
    bndu[i]=+INFINITY;
    for(int j=0;j<n;j++)
      hx(i,j)=qp._cons[j].dot(invA*qp._cons[i]);
  }

  //solve
  if(n > 0) {
    alglib::minqpstate state;
    alglib::minqpreport rep;
    alglib::minqpcreate(n,state);
    alglib::minqpsetalgoquickqp(state,0,0,0,100000000,true);
    alglib::minqpsetquadraticterm(state,hx);
    alglib::minqpsetlinearterm(state,b);
    alglib::minqpsetbc(state,bndl,bndu);
    alglib::minqpoptimize(state);
    alglib::minqpresults(state,x,rep);
    //ASSERT(rep.terminationtype == 4)
  }

  //recover primal variables
  qp._result=-qp._b;
  for(int i=0;i<n;i++)
    qp._result+=qp._cons[i]*x[i];
  qp._result.array()*=invA.diagonal().array();
}
void QP::runExample()
{
#define N 100
#define M 10
  QPProb qp;
  //random objective
  qp._A=Eigen::DiagonalMatrix<double,-1>(N);
  qp._b.setRandom(N);
  for(int i=0;i<N;i++)
    qp._A.diagonal()[i]=rand()/(double)RAND_MAX;
  //random constraints
  qp._cons.resize(M);
  qp._rhs.resize(M);
  for(int i=0;i<M;i++) {
    qp._cons[i].resize(N);
    Eigen::VectorXd tmp;
    tmp.setRandom(N);
    for(int j=0;j<N;j++)
      qp._cons[i].coeffRef(j)=tmp[j];
    qp._rhs[i]=rand()/(double)RAND_MAX*2-1;
  }
  //solve primal
  solveQPPrimal(qp);
  cout << "primal energy: " << qp._result.dot(qp._A*qp._result/2+qp._b) << endl;
  for(int i=0;i<M;i++) {
    double val=qp._cons[i].dot(qp._result)-qp._rhs[i];
    cout << "Constraint val: " << val << " violation: " << min<double>(val,0) << endl;
  }
  //solve dual
  QPProb qp2=qp;
  solveQPDual(qp2);
  cout << "dual energy: " << qp2._result.dot(qp2._A*qp2._result/2+qp2._b) << endl;
  for(int i=0;i<M;i++) {
    double val=qp2._cons[i].dot(qp2._result)-qp2._rhs[i];
    cout << "Constraint val: " << val << " violation: " << min<double>(val,0) << endl;
  }
  cout << "Primal-Dual error: " << (qp._result-qp2._result).norm() << endl;
#undef M
#undef N
}

#undef ITERV
#undef ITERE
#undef ZEROV
#undef ZEROE
#undef ASSIGNV
#undef ASSIGNE
#undef ASSEMBLEV
#undef ASSEMBLEE
