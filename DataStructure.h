#ifndef DATA_STRUCTURE_H
#define DATA_STRUCTURE_H

#include "DataUtility.h"
#include <set>
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <boost/shared_array.hpp>

using namespace std;
struct Operation;
struct ObjectFeature;

//CAD-120 data structure
struct Skeleton {
  friend struct CRF;
  Skeleton();
  Skeleton(int id);
  Skeleton(const string& line);
  void writeVTK(const string& dir) const;
  bool operator<(const Skeleton& other) const;
  bool valid() const;
  template<class Archive>
  void serialize(Archive& ar,const unsigned int version) {
    ar & _joints & _jointConfs & _poses & _posConfs & _idInActivity;
  }
private:
  vector<Eigen::Affine3d> _joints;
  vector<Eigen::Vector2i> _jointConfs;
  vector<Eigen::Vector3d> _poses;
  vector<int> _posConfs;
  int _idInActivity;
};
struct RGBDImage {
  friend struct CRF;
  RGBDImage();
  RGBDImage(int id);
  void reset(const string& line);
  bool read(istream& is);
  bool write(ostream& is) const;
  vector<Eigen::Vector3d> getPC(const ObjectFeature& feature) const;
  void writePCVTK(const string& dir,const vector<const ObjectFeature*>& objectPCs) const;
  bool operator<(const RGBDImage& other) const;
  bool empty() const;
  int id() const;
private:
  static int size();
  static int at(int x,int y,int d);
  void createPointCloud();
  int _xRes,_yRes;
  vector<int> _data;
  vector<Eigen::Vector3d> _pos;
  vector<Eigen::Vector4d> _color;
  int _idInActivity;
};
struct ObjectFeature {
  void compute(const RGBDImage& img);
  template<class Archive>
  void serialize(Archive& ar,const unsigned int version) {
    ar & _feature & _PC & _centroid & _eigVals & _normal;
  }
  Eigen::VectorXd _feature;
  Eigen::VectorXi _PC;
  Eigen::Vector3d _centroid;
  Eigen::Vector3d _eigVals;
  Eigen::Vector3d _normal;
};
struct Object {
  friend struct CRF;
  friend struct Activity;
  Object();
  Object(int id);
  Object(const string& line);
  bool readObject(string objPath) ;
  bool readObjectPC(string objPath);
  bool operator<(const Object& other) const;
  const ObjectFeature* getFeat(int frm) const;
  int id() const;
  template<class Archive>
  void serialize(Archive& ar,const unsigned int version) {
    ar & _features & _type & _idInActivity;
  }
private:
  map<int,ObjectFeature> _features;
  string _type;
  int _idInActivity;
};
struct SubActivity {
  friend struct CRF;
  SubActivity();
  SubActivity(const string& line);
  bool operator<(const SubActivity& other) const;
  bool hasAffordance(int id) const;
  int endFrame() const;
  template<class Archive>
  void serialize(Archive& ar,const unsigned int version) {
    ar & _affordance & _type & _start & _end;
  }
private:
  vector<string> _affordance;
  string _type;
  int _start;
  int _end;
  //LP parameter index
  vector<int> _LPIndexAffordance;
  int _LPIndexType;
};
struct Activity {
  friend struct CRF;
  Activity();
  Activity(const string& line);
  bool readGlobalFrame(const string& file);
  bool readFrames(const string& dirRGBD,const string& dirObj,Operation& op);
  bool readSkeletons(const string& file);
  bool readRGBDs(const string& file,Operation& op);
  bool readCluster(const string& line);
  bool addSubActivity(const string& line);
  bool operator<(const Activity& other) const;
  bool operator<(const string& other) const;
  const Skeleton* getSkel(int id) const;
  const vector<const ObjectFeature*> getObjectFeats(int frm) const;
  const string& id() const;
  bool valid() const;
  template<class Archive>
  void serialize(Archive& ar,const unsigned int version) {
    ar & _objects & _subActivities & _skeletons & _clusters & _gTrans & _type & _subject & _id;
  }
private:
  int nrEffectiveFrame() const;
  set<Object> _objects;
  set<SubActivity> _subActivities;
  set<Skeleton> _skeletons;
  vector<vector<int> > _clusters;
  Eigen::Affine3d _gTrans;
  string _type;
  string _subject;
  string _id;
};
struct Activities {
  friend struct CRF;
  Activities();
  void read(const string& path);
  void write(const string& path);
  bool readDataActMap(const string& dir);
  bool readLabelFile(const string& dir);
  bool readSegmentsFile(const string& segmentsFile);
  bool readGlobalTransforms(const string& dir);
  bool readFrames(Operation& op);
  void merge(const Activities& acts);
  Activity getActivity(const string& id) const;
  const set<Activity>& getActivities() const;
  template<class Archive>
  void serialize(Archive& ar,const unsigned int version) {
    ar & _activities & _classMap & _affMap;
  }
private:
  set<Activity> _activities;
  map<string,string> _classMap,_affMap;
  map<string,string> _pathRGBD,_pathAnnotation;
};
//Operation on data structure
struct Operation {
  virtual ~Operation() {}
  virtual void init(const Activity& act) {}
  virtual void onActivity(const Activity& act,const RGBDImage& img) {}
};
struct WriteOperation : public Operation {
  WriteOperation(int interval=10);
  virtual void init(const Activity& act) override;
  virtual void onActivity(const Activity& act,const RGBDImage& img) override;
private:
  string _path;
  int _interval;
};

#endif
