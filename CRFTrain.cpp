#include "CRF.h"

double computeLoss(const vector<double>& a,const vector<double>& b) {
  ASSERT(a.size() == b.size())
  double ret=0;
  for(int i=0;i<(int)a.size();i++)
    ret+=abs(a[i]-b[i]);
  return ret;
}
vector<double> computeMaxVio(LPProb& lp,const Eigen::VectorXd& w) {
  //equivalent to solving LP by GraphCut
  for(int i=0;i<(int)lp._coef.size();i++) {
    LPCoef& coef=lp._coef[i];
    coef._coef=coef._feature.dot(w.block(coef._weightOffset,0,coef._feature.size(),1));
  }
  for(map<int,map<int,LPCoef> >::iterator beg2=lp._quadraticTerms.begin(),end2=lp._quadraticTerms.end();beg2!=end2;beg2++)
    for(map<int,LPCoef>::iterator beg=beg2->second.begin(),end=beg2->second.end();beg!=end;beg++) {
      LPCoef& coef=beg->second;
      coef._coef=coef._feature.dot(w.block(coef._weightOffset,0,coef._feature.size(),1));
    }
  LP::solveGraphCutQPBO(lp);
  vector<double> ret(lp._result.size());
  for(int i=0;i<(int)ret.size();i++)
    ret[i]=lp._result[i];
  return ret;
}
Eigen::SparseVector<double> computeFeature(const LPProb& lp,const vector<double>& Y,int size) {
  vector<pair<int,double> > features;
  //y could only be 0/0.5/1
  double val;
  for(int i=0;i<(int)lp._coef.size();i++)
    if((val=Y[i]) > 0) {
      const LPCoef& f=lp._coef[i];
      for(int off=0;off<f._feature.size();off++)
        features.push_back(make_pair(off+f._weightOffset,f._feature[off]*val));
    }
  for(map<int,map<int,LPCoef> >::const_iterator beg2=lp._quadraticTerms.begin(),end2=lp._quadraticTerms.end();beg2!=end2;beg2++)
    for(map<int,LPCoef>::const_iterator beg=beg2->second.begin(),end=beg2->second.end();beg!=end;beg++)
    if((val=Y[beg2->first]*Y[beg->first]) > 0) {
      const LPCoef& f=beg->second;
      for(int off=0;off<f._feature.size();off++)
        features.push_back(make_pair(off+f._weightOffset,f._feature[off]*val));
    }
  //copy to SVECTOR
  Eigen::SparseVector<double> ret;
  ret.resize(size);
  for(int i=0;i<(int)features.size();i++)
    ret.coeffRef(features[i].first)=features[i].second;
  return ret;
}
void CRF::buildTrainingSamples(vector<LPProb>& trainingSamples, const Activities& acts)
{
  //build training samples
  for(set<Activity>::const_iterator beg=acts._activities.begin(),end=acts._activities.end();beg!=end;beg++) {
    //add terms
    vector<boost::shared_ptr<Term> > terms;
    addActivity(*beg,terms);
    //build terms
    LPProb lp;
    for(int i=0; i<(int)terms.size(); i++)
      terms[i]->buildLPIndex(lp,lp._nameBinding);
    for(int i=0; i<(int)terms.size(); i++)
      terms[i]->buildLPProb(lp);
    //problems
    trainingSamples.push_back(lp);
  }
}
void CRF::train(double C,double eps,const string& path,const Activities& acts)
{
  //build training samples
  vector<LPProb> trainingSamples;
  buildTrainingSamples(trainingSamples, acts);

  //initialize working set
  int nrT=(int)trainingSamples.size();
  vector<vector<int> > S(nrT);
  vector<Eigen::SparseVector<double> > features;
  for(int i=0;i<nrT;i++)
    features.push_back(computeFeature(trainingSamples[i],trainingSamples[i]._Y,_nrW+nrT));

  //initialize weight and slack variable
  QPProb prob;
  prob._A.setIdentity(_nrW+nrT);
  prob._A.diagonal().block(_nrW,0,nrT,1).setZero();
  prob._b.setZero(_nrW+nrT);
  prob._b.block(_nrW,0,nrT,1).setConstant(C/(double)nrT);
  prob._result.setZero(_nrW+nrT);
  prob._rhs.assign(nrT,0);
  prob._cons.resize(nrT);
  for(int i=0;i<nrT;i++){
    prob._cons[i].resize(_nrW+nrT);
    prob._cons[i].coeffRef(_nrW+i)=1;
  }

  //main training algor
  bool needMore=true;
  for(int iter=0;needMore && iter<1000;iter++) {
    //augment active set
    needMore=false;
    for(int i=0;i<nrT;i++) {
      //find the most violated one
      LPProb& lp=trainingSamples[i];
      vector<double> Y=computeMaxVio(lp,prob._result);
      double loss=computeLoss(Y,lp._Y);
      //compute constraint to see if it's violated
      Eigen::SparseVector<double> ccoef=features[i]-computeFeature(lp,Y,_nrW+nrT)+prob._cons[i];
      if(ccoef.dot(prob._result) < loss-eps) {
        prob._cons.push_back(ccoef);
        prob._rhs.push_back(loss);
        needMore=true;
      }
    }
    //solve for new weight and C
    cout << "Iter " << iter << " has " << prob._rhs.size() << " constraints!" << endl;
    QP::solveQPDual(prob);
    //QP::solveQPPrimal(prob);
  }

  //assign weights
  assign(prob._result.block(0,0,_nrW,1));
  write(path);
}
