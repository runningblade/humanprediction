#include "constants.h"
#include "DataStructure.h"
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>
//#include <boost/archive/text_oarchive.hpp>
//#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/map.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>

template <typename T>
void removeRedundant(set<T>& s,int nr)
{
  for(typename set<T>::const_iterator beg=s.begin(); beg!=s.end(); beg++)
    if(T(nr) < *beg) {
      s.erase(*beg);
      beg=s.begin();
    }
}
template <typename T>
void removeRedundant(map<int,T>& s,int nr)
{
  for(typename map<int,T>::const_iterator beg=s.begin(); beg!=s.end(); beg++)
    if(nr < beg->first) {
      s.erase(beg->first);
      beg=s.begin();
    }
}
bool canRead(istream& is,string& line)
{
  if(!getline(is,line))
    return false;
  if(line.empty() || (line.size() >= 3 && line.substr(0,3) == "END"))
    return false;
  return true;
}
bool isEmptyStream(istream& is,string& line)
{
  while(!is.eof() && getline(is,line))
    if(!line.empty() && !(line.size() >= 3 && line.substr(0,3) == "END"))
      return false;
  return true;
}

//Skeleton
Skeleton::Skeleton()
{
  _joints.resize(JOINT_NUM);
  _jointConfs.resize(JOINT_NUM);
  _poses.resize(POS_JOINT_NUM);
  _posConfs.resize(POS_JOINT_NUM);
}
Skeleton::Skeleton(int id):_idInActivity(id)
{
  _joints.resize(JOINT_NUM);
  _jointConfs.resize(JOINT_NUM);
  _poses.resize(POS_JOINT_NUM);
  _posConfs.resize(POS_JOINT_NUM);
}
Skeleton::Skeleton(const string& line)
{
  boost::char_separator<char> sep(",");
  boost::tokenizer<boost::char_separator<char> > tok(line,sep);
  boost::tokenizer<boost::char_separator<char> >::iterator beg=tok.begin();
  if(beg == tok.end())
    return;
  _idInActivity=atoi((beg++)->c_str());
  if(distance(beg,tok.end()) != JOINT_NUM*(JOINT_DATA_NUM+JOINT_DATA_TYPE_NUM) + POS_JOINT_NUM*(POS_JOINT_DATA_NUM+1))
    return;
  //to world
  Eigen::Affine3d toWorld;
  toWorld.linear().setZero();
  toWorld.linear()(0,0)=toWorld.linear()(1,2)=toWorld.linear()(2,1)=1;
  //read joints
  _joints.resize(JOINT_NUM);
  _jointConfs.resize(JOINT_NUM);
  for(int i=0; i<JOINT_NUM; i++) {
    for(int c=0; c<3; c++)
      for(int r=0; r<3; r++)
        _joints[i].linear()(r,c)=atof((beg++)->c_str());
    _jointConfs[i][0]=atoi((beg++)->c_str());
    for(int c=0; c<3; c++)
      _joints[i].translation()(c)=atof((beg++)->c_str());
    _jointConfs[i][1]=atoi((beg++)->c_str());
    //transform to world
    _joints[i]=toWorld*_joints[i];
  }
  //read positions
  _poses.resize(POS_JOINT_NUM);
  _posConfs.resize(POS_JOINT_NUM);
  for(int i=0; i<POS_JOINT_NUM; i++) {
    for(int c=0; c<3; c++)
      _poses[i][c]=atof((beg++)->c_str());
    _posConfs[i]=atoi((beg++)->c_str());
    //transform to world
    _poses[i]=toWorld*_poses[i];
  }
}
void Skeleton::writeVTK(const string& dir) const
{
  ostringstream oss;
  oss << dir << "/frame" << _idInActivity << ".vtk";
  VTKWriter<double> os("Skeleton",oss.str(),true);

  static double len=10.0f;
  vector<Eigen::Vector3d> vss;
  vector<Eigen::Vector2i> iss;
  vector<Eigen::Vector4d> css;
  for(int i=0; i<JOINT_NUM; i++) {
    int nrv=(int)vss.size();
    vss.push_back(_joints[i].translation());
    vss.push_back(_joints[i].translation()+_joints[i].rotation().col(0)*len);
    vss.push_back(_joints[i].translation()+_joints[i].rotation().col(1)*len);
    vss.push_back(_joints[i].translation()+_joints[i].rotation().col(2)*len);
    css.push_back(Eigen::Vector4d(0.5,0.5,0.5,0.5));
    css.push_back(Eigen::Vector4d(1,0,0,1));
    css.push_back(Eigen::Vector4d(0,1,0,1));
    css.push_back(Eigen::Vector4d(0,0,1,1));
    iss.push_back(Eigen::Vector2i(nrv,nrv+1));
    iss.push_back(Eigen::Vector2i(nrv,nrv+2));
    iss.push_back(Eigen::Vector2i(nrv,nrv+3));
  }
  int offPose=(int)vss.size();
  for(int i=0; i<POS_JOINT_NUM; i++) {
    vss.push_back(_poses[i]);
    css.push_back(Eigen::Vector4d(0.5,0.5,0.5,0.5));
  }
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(iss.begin(),iss.end(),VTKWriter<double>::LINE);
  os.appendCustomPointColorData("Color",css.begin(),css.end());

  iss.clear();
  iss.push_back(Eigen::Vector2i(HEAD_JOINT_NUM*4,NECK_JOINT_NUM*4));
  iss.push_back(Eigen::Vector2i(NECK_JOINT_NUM*4,TORSO_JOINT_NUM*4));
  //left hand
  iss.push_back(Eigen::Vector2i(NECK_JOINT_NUM*4,LEFT_SHOULDER_JOINT_NUM*4));
  iss.push_back(Eigen::Vector2i(LEFT_SHOULDER_JOINT_NUM*4,LEFT_ELBOW_JOINT_NUM*4));
  iss.push_back(Eigen::Vector2i(LEFT_ELBOW_JOINT_NUM*4,offPose+POS_LEFT_HAND_NUM));
  //right hand
  iss.push_back(Eigen::Vector2i(NECK_JOINT_NUM*4,RIGHT_SHOULDER_JOINT_NUM*4));
  iss.push_back(Eigen::Vector2i(RIGHT_SHOULDER_JOINT_NUM*4,RIGHT_ELBOW_JOINT_NUM*4));
  iss.push_back(Eigen::Vector2i(RIGHT_ELBOW_JOINT_NUM*4,offPose+POS_RIGHT_HAND_NUM));
  //left leg
  iss.push_back(Eigen::Vector2i(TORSO_JOINT_NUM*4,LEFT_HIP_JOINT_NUM*4));
  iss.push_back(Eigen::Vector2i(LEFT_HIP_JOINT_NUM*4,LEFT_KNEE_JOINT_NUM*4));
  iss.push_back(Eigen::Vector2i(LEFT_KNEE_JOINT_NUM*4,offPose+POS_LEFT_FOOT_NUM));
  //right leg
  iss.push_back(Eigen::Vector2i(TORSO_JOINT_NUM*4,RIGHT_HIP_JOINT_NUM*4));
  iss.push_back(Eigen::Vector2i(RIGHT_HIP_JOINT_NUM*4,RIGHT_KNEE_JOINT_NUM*4));
  iss.push_back(Eigen::Vector2i(RIGHT_KNEE_JOINT_NUM*4,offPose+POS_RIGHT_FOOT_NUM));
  os.appendCells(iss.begin(),iss.end(),VTKWriter<double>::LINE);
}
bool Skeleton::operator<(const Skeleton& other) const
{
  return _idInActivity < other._idInActivity;
}
bool Skeleton::valid() const
{
  return !_joints.empty();
}
//RGBDImage
RGBDImage::RGBDImage() {}
RGBDImage::RGBDImage(int id):_idInActivity(id) {}
void RGBDImage::reset(const string& line)
{
  boost::char_separator<char> sep(",");
  boost::tokenizer<boost::char_separator<char> > tok(line,sep);
  boost::tokenizer<boost::char_separator<char> >::iterator beg=tok.begin();
  if(beg == tok.end())
    return;
  _idInActivity=atoi((beg++)->c_str());
  if(distance(beg,tok.end()) != size()) {
    _data.resize(0);
    return;
  }
  if((int)_data.size() != size())
    _data.resize(size());
  for(int i=0; i<(int)_data.size(); i++)
    _data[i]=atoi((beg++)->c_str());
  //for(int y=0; y<Y_RES; y++)
  //  for(int x=0; x<X_RES; x++)
  //    for(int d=0; d<RGBD_data; d++)
  //      _data[at(x,y,d)]=atoi((beg++)->c_str());
  createPointCloud();
}
bool RGBDImage::read(istream& is)
{
  is.read((char*)&_idInActivity,sizeof(int));
  if(_idInActivity <= 0)
    return false;
  if((int)_data.size() != size())
    _data.resize(size());
  is.read((char*)&(_data[0]),sizeof(int)*_data.size());
  createPointCloud();
  return is.good();
}
bool RGBDImage::write(ostream& os) const
{
  os.write((char*)&_idInActivity,sizeof(int));
  os.write((char*)&(_data[0]),sizeof(int)*_data.size());
  return os.good();
}
vector<Eigen::Vector3d> RGBDImage::getPC(const ObjectFeature& feature) const
{
  vector<Eigen::Vector3d> ret;
  for(int i=0; i<(int)feature._PC.size(); i++)
    ret.push_back(_pos[feature._PC[i]]);
  return ret;
}
void RGBDImage::writePCVTK(const string& dir,const vector<const ObjectFeature*>& objectPCs) const
{
  //label objects
  vector<double> objectId(_pos.size(),0);
  for(int i=0; i<(int)objectPCs.size(); i++)
    for(int j=0; objectPCs[i] && j<objectPCs[i]->_PC.size(); j++)
      objectId[(*objectPCs[i])._PC[j]]=(double)(i+1);

  //VTKWriter
  ostringstream oss;
  oss << dir << "/frame" << _idInActivity << ".vtk";
  VTKWriter<double> os("PointCloud",oss.str(),true);
  os.appendPoints(_pos.begin(),_pos.end());
  os.appendCells(VTKWriter<double>::IteratorIndex<Eigen::Vector3i>(0,0,0),
                 VTKWriter<double>::IteratorIndex<Eigen::Vector3i>((int)_pos.size(),0,0),
                 VTKWriter<double>::POINT);
  os.appendCustomPointData("ObjectId",objectId.begin(),objectId.end());
  os.appendCustomPointColorData("Color",_color.begin(),_color.end());
}
bool RGBDImage::operator<(const RGBDImage& other) const
{
  return _idInActivity < other._idInActivity;
}
bool RGBDImage::empty() const
{
  return _data.empty();
}
int RGBDImage::id() const
{
  return _idInActivity;
}
int RGBDImage::size()
{
  return X_RES*Y_RES*RGBD_data;
}
int RGBDImage::at(int x,int y,int d)
{
  return (y*X_RES+x)*RGBD_data+d;
}
void RGBDImage::createPointCloud()
{
  if((int)_pos.size() != size()/RGBD_data)
    _pos.resize(size()/RGBD_data);
  if((int)_color.size() != size()/RGBD_data)
    _color.resize(size()/RGBD_data);
  #pragma omp parallel for
  for(int y=0; y<Y_RES; y++) {
    for(int x=0; x<X_RES; x++) {
      int index=y*X_RES+x;
      _pos[index].y()=_data[at(x,y,3)];
      _pos[index].x()=(x - 640 * 0.5) * _pos[index].y() * 1.1147 / 640.0;
      _pos[index].z()=(480 * 0.5 - y) * _pos[index].y() * 0.8336 / 480.0;

      _color[index][0]=_data[at(x,y,0)]/ 255.0;
      _color[index][1]=_data[at(x,y,1)]/ 255.0;
      _color[index][2]=_data[at(x,y,2)]/ 255.0;
      _color[index][3]=1;
    }
  }
}
//Object
void ObjectFeature::compute(const RGBDImage& img)
{
  vector<Eigen::Vector3d> PC=img.getPC(*this);
  //center
  _centroid.setZero();
  for(int i=0; i<(int)PC.size(); i++)
    _centroid+=PC[i];
  _centroid/=max<int>(1,(int)PC.size());
  //covariance
  Eigen::Matrix3d covariance;
  covariance.setZero();
  for(int i=0; i<(int)PC.size(); i++)
    covariance+=(PC[i]-_centroid)*(PC[i]-_centroid).transpose();
  covariance/=max<int>(1,(int)PC.size());
  //clear memory
  _PC.setZero(0);
  //normal
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> eig(covariance);
  _eigVals=eig.eigenvalues();
  _normal=eig.eigenvectors().col(0);
  ASSERT(_eigVals[0] <= _eigVals[1] && _eigVals[1] <= _eigVals[2]);
  if((Eigen::Vector3d::Zero()-_centroid).dot(_normal) < 0)
    _normal*=-1;
}
Object::Object() {}
Object::Object(int id):_idInActivity(id) {}
Object::Object(const string& line)
{
  int sep=(int)line.find(":",0);
  _type=line.substr(sep+1);
  _idInActivity=atoi(line.substr(0,sep).c_str());
}
bool Object::readObject(string objPath)
{
  objPath+="_obj"+boost::lexical_cast<string>(_idInActivity)+".txt";
  _features.clear();
  string line;
  ASSERT(boost::filesystem::exists(objPath))
  boost::filesystem::ifstream is(objPath);
  while(canRead(is,line)) {
    boost::char_separator<char> sep(",");
    boost::tokenizer<boost::char_separator<char> > tok(line,sep);
    boost::tokenizer<boost::char_separator<char> >::iterator beg=tok.begin();
    if(distance(beg,tok.end()) == NUM_OBJ_FEATS) {
      Eigen::VectorXd& feats=_features[atoi((beg++)->c_str())]._feature;
      feats.resize(NUM_OBJ_FEATS-1);
      for(int i=0; i<NUM_OBJ_FEATS-1; i++)
        feats[i]=atof((beg++)->c_str());
    } else continue;
  }
  return isEmptyStream(is,line);
}
bool Object::readObjectPC(string objPath)
{
  objPath+="_obj"+boost::lexical_cast<string>(_idInActivity)+".txt";
  string line;
  ASSERT(boost::filesystem::exists(objPath))
  boost::filesystem::ifstream is(objPath);
  while(canRead(is,line)) {
    boost::char_separator<char> sep(",");
    boost::tokenizer<boost::char_separator<char> > tok(line,sep);
    boost::tokenizer<boost::char_separator<char> >::iterator beg=tok.begin();
    //skip activity id
    if(beg == tok.end())
      continue;
    beg++;
    //header
    if(beg == tok.end())
      return false;
    map<int,ObjectFeature>::iterator it=_features.find(atoi((beg++)->c_str()));
    if(it == _features.end() || beg == tok.end() || atoi((beg++)->c_str()) != _idInActivity)
      return false;
    //read
    Eigen::VectorXi& PC=it->second._PC;
    PC.setZero(distance(beg,tok.end()));
    for(int i=0; beg != tok.end(); beg++,i++) {
      PC[i]=atoi(beg->c_str());
      ASSERT(PC[i] < X_RES*Y_RES);
    }
  }
  return isEmptyStream(is,line);
}
bool Object::operator<(const Object& other) const
{
  return _idInActivity < other._idInActivity;
}
const ObjectFeature* Object::getFeat(int frm) const
{
  map<int,ObjectFeature>::const_iterator it=_features.find(frm);
  if(it != _features.end())
    return &(it->second);
  return NULL;
}
int Object::id() const
{
  return _idInActivity;
}
//SubActivity
SubActivity::SubActivity() {}
SubActivity::SubActivity(const string& line)
{
  boost::char_separator<char> sep(",");
  boost::tokenizer<boost::char_separator<char> > tok(line,sep);
  boost::tokenizer<boost::char_separator<char> >::iterator beg=tok.begin();
  if(beg == tok.end())
    return;
  _start=atoi((beg++)->c_str());
  if(beg == tok.end())
    return;
  _end=atoi((beg++)->c_str());
  if(beg == tok.end())
    return;
  ASSERT(_start <= _end);
  _type=(beg++)->c_str();
  if(beg == tok.end())
    return;
  _affordance.push_back("");	//index start from 1
  while(beg != tok.end())
    _affordance.push_back(*beg++);
}
bool SubActivity::operator<(const SubActivity& other) const
{
  ASSERT(_end < other._start || other._end < _start);
  return _start < other._start;
}
bool SubActivity::hasAffordance(int id) const
{
  return id < (int)_affordance.size();
}
int SubActivity::endFrame() const
{
  return _end;
}
//Activity
Activity::Activity() {}
Activity::Activity(const string& line)
{
  boost::char_separator<char> sep(",");
  boost::tokenizer<boost::char_separator<char> > tok(line,sep);
  boost::tokenizer<boost::char_separator<char> >::iterator beg=tok.begin();
  if(beg == tok.end())
    return;
  _id=(beg++)->c_str();
  if(beg == tok.end())
    return;
  _type=(beg++)->c_str();
  if(beg == tok.end())
    return;
  _subject=(beg++)->c_str();
  if(beg == tok.end())
    return;
  while(beg != tok.end())
    _objects.insert(Object(*beg++));
}
bool Activity::readGlobalFrame(const string& file)
{
  char comma;
  double zero0,zero1,zero2,one;
  if(!boost::filesystem::exists(file))
    return false;
  boost::filesystem::ifstream is(file);
  is >> _gTrans.linear()(0,0) >> comma >> _gTrans.linear()(0,1) >> comma >> _gTrans.linear()(0,2) >> comma >> _gTrans.translation()[0];
  is >> _gTrans.linear()(1,0) >> comma >> _gTrans.linear()(1,1) >> comma >> _gTrans.linear()(1,2) >> comma >> _gTrans.translation()[1];
  is >> _gTrans.linear()(2,0) >> comma >> _gTrans.linear()(2,1) >> comma >> _gTrans.linear()(2,2) >> comma >> _gTrans.translation()[2];
  is >> zero0 >> comma >> zero1 >> comma >> zero2 >> comma >> one;
  return (zero0 == 0 && zero1 == 0 && zero2 == 0 && one == 1);
}
bool Activity::readFrames(const string& dirRGBD,const string& dirObj,Operation& op)
{
  cout << "Reading activity: " << _id << endl;
  //read skeletons
  if(!readSkeletons(dirObj+"/"+_id+".txt"))
    return false;
  //read objects
  for(set<Object>::const_iterator beg=_objects.begin(),end=_objects.end(); beg!=end; beg++)
    if(!const_cast<Object&>(*beg).readObject(dirObj+"/"+_id) || !const_cast<Object&>(*beg).readObjectPC(dirObj+"/objects/"+_id))
      return false;
  //read RGBD images
  op.init(*this);
  if(!readRGBDs(dirRGBD+"/"+_id+"_rgbd.txt",op))
    return false;
  //synchronize frames
  int nrFrame=nrEffectiveFrame();
  cout << "Found " << nrFrame << " valid frames!" << endl;
  removeRedundant(_skeletons,nrFrame);
  for(set<Object>::const_iterator beg=_objects.begin(),end=_objects.end(); beg!=end; beg++)
    removeRedundant(const_cast<Object&>(*beg)._features,nrFrame);
  for(set<SubActivity>::const_iterator beg=_subActivities.begin(),end=_subActivities.end(); beg!=end; beg++)
    ASSERT(beg->endFrame() <= nrFrame)
    return true;
}
bool Activity::readSkeletons(const string& file)
{
  _skeletons.clear();
  string line;
  boost::filesystem::ifstream is(file);
  while(canRead(is,line)) {
    Skeleton skel(line);
    if(!skel.valid() || _skeletons.find(skel) != _skeletons.end())
      return false;
    else _skeletons.insert(skel);
  }
  return isEmptyStream(is,line);
}
bool convert(const string& file,const string& fileTo)
{
  cout << "Converting RGBD to binary format!" << endl;
  boost::filesystem::ifstream is(file);
  boost::filesystem::ofstream os(fileTo,ios::binary);

  int nrFrameRead=0;
  string line;
  RGBDImage img;
  while(canRead(is,line)) {
    img.reset(line);
    if(img.empty()) {
      return false;
    } else {
      img.write(os);
      if((nrFrameRead++)%10 == 0)
        cout << "Converted " << nrFrameRead << " frames!" << endl;
    }
  }
  int endTag=-1;
  os.write((char*)&endTag,sizeof(int));
  //remove original file
  cout << "Removing original RGBD file" << endl;
  boost::filesystem::path path(file);
  boost::filesystem::remove(path);
  return true;
}
bool Activity::readRGBDs(const string& file,Operation& op)
{
  boost::filesystem::path rgbdBinary(file);
  rgbdBinary.replace_extension(".bin");
  if(!boost::filesystem::exists(rgbdBinary))
    ASSERT(convert(file,rgbdBinary.string()))

    int nrFrameRead=0;
  RGBDImage img;
  boost::filesystem::ifstream is(rgbdBinary,ios::binary);
  while(img.read(is)) {
    //perform user activity
    op.onActivity(*this,img);
    //build object feature
    vector<const ObjectFeature*> feats=getObjectFeats(img.id());
    for(int i=0; i<(int)feats.size(); i++)
      if(feats[i])const_cast<ObjectFeature*>(feats[i])->compute(img);
    if((nrFrameRead++)%10 == 0)
      cout << "Read " << nrFrameRead << " frames!" << endl;
  }
  return true;
}
bool Activity::readCluster(const string& line)
{
  boost::char_separator<char> sep(":,");
  boost::tokenizer<boost::char_separator<char> > tok(line,sep);
  boost::tokenizer<boost::char_separator<char> >::iterator beg=tok.begin();
  if(beg == tok.end())
    return false;
  if(atoi((*beg++).c_str()) != (int)_clusters.size()+1)
    return false;
  if(beg == tok.end())
    return false;
  _clusters.push_back(vector<int>());
  while(beg != tok.end())
    _clusters.back().push_back(atoi((*beg++).c_str()));
  return true;
}
bool Activity::addSubActivity(const string& line)
{
  SubActivity act(line);
  for(set<Object>::const_iterator beg=_objects.begin(),end=_objects.end(); beg!=end; beg++)
    if(!act.hasAffordance(beg->id()))
      return false;
  _subActivities.insert(act);
  return true;
}
bool Activity::operator<(const Activity& other) const
{
  return _id < other._id;
}
bool Activity::operator<(const string& other) const
{
  return _id < other;
}
const Skeleton* Activity::getSkel(int id) const
{
  set<Skeleton>::const_iterator it=_skeletons.find(Skeleton(id));
  if(it != _skeletons.end())
    return &*it;
  else return NULL;
}
const vector<const ObjectFeature*> Activity::getObjectFeats(int frm) const
{
  vector<const ObjectFeature*> ret(_objects.size()+1,NULL);
  for(set<Object>::const_iterator beg=_objects.begin(),end=_objects.end(); beg!=end; beg++)
    ret[beg->id()]=beg->getFeat(frm);
  return ret;
}
const string& Activity::id() const
{
  return _id;
}
bool Activity::valid() const
{
  return !_objects.empty();
}
int Activity::nrEffectiveFrame() const
{
  int nrFrame=1;
  while(true) {
    if(_skeletons.find(Skeleton(nrFrame)) == _skeletons.end())
      return nrFrame-1;
    for(set<Object>::const_iterator beg=_objects.begin(),end=_objects.end(); beg!=end; beg++)
      if(!beg->getFeat(nrFrame))
        return nrFrame-1;
    nrFrame++;
  }
  return 0;
}
//Activities
Activities::Activities()
{
  _classMap["1"] = "reaching";
  _classMap["2"] = "moving";
  _classMap["3"] = "pouring";
  _classMap["4"] = "eating";
  _classMap["5"] = "drinking";
  _classMap["6"] = "opening";
  _classMap["7"] = "placing";
  _classMap["8"] = "closing";
  _classMap["9"] = "null";
  _classMap["10"] = "cleaning";

  _affMap["1"] =  "movable";
  _affMap["2"] =  "stationary";
  _affMap["3"] =  "reachable";
  _affMap["4"] =  "pourable";
  _affMap["5"] =  "pourto";
  _affMap["6"] =  "containable";
  _affMap["7"] =  "drinkable";
  _affMap["8"] =  "openable";
  _affMap["9"] =  "placeable";
  _affMap["10"] =  "closeable";
  _affMap["11"] =  "cleanable";
  _affMap["12"] =  "cleaner";
}
void Activities::read(const string& path)
{
  boost::filesystem::ifstream is(path,ios::binary);
  boost::archive::binary_iarchive ar(is);
  ar & *this;
}
void Activities::write(const string& path)
{
  boost::filesystem::ofstream os(path,ios::binary);
  boost::archive::binary_oarchive ar(os);
  ar & *this;
}
bool Activities::readDataActMap(const string& dir)
{
  string line;
  _activities.clear();
  _pathRGBD.clear();
  _pathAnnotation.clear();
  for(boost::filesystem::directory_iterator beg(dir+"_annotations"); beg!=boost::filesystem::directory_iterator(); beg++) {
    boost::filesystem::path path(*beg);
    if(boost::filesystem::exists(path/"activityLabel.txt")) {
      boost::filesystem::ifstream is(path/"activityLabel.txt");
      while(canRead(is,line)) {
        Activity act(line);
        if(act.valid()) {
          _pathRGBD[act.id()]=_pathAnnotation[act.id()]=path.string();
          boost::algorithm::replace_all(_pathRGBD[act.id()],"_annotations","_rgbd_rawtext");
          ASSERT(boost::filesystem::exists(_pathRGBD[act.id()]));
          _activities.insert(act);
        } else return false;
      }
      if(!isEmptyStream(is,line))
        return false;
    }
  }
  return true;
}
bool Activities::readLabelFile(const string& dir)
{
  string line;
  for(boost::filesystem::directory_iterator beg(dir+"_annotations"); beg!=boost::filesystem::directory_iterator(); beg++) {
    boost::filesystem::path path(*beg);
    path/="labeling.txt";
    boost::filesystem::ifstream is(path);
    set<Activity>::const_iterator it;
    while(canRead(is,line))
      if((it=_activities.find(line.substr(0,10))) == _activities.end())
        continue;
      else if(!const_cast<Activity&>(*it).addSubActivity(line.substr(11)))
        return false;
    if(!isEmptyStream(is,line))
      return false;
  }
  return true;
}
bool Activities::readSegmentsFile(const string& segmentsFile)
{
  string line;
  boost::filesystem::ifstream is(segmentsFile);
  set<Activity>::const_iterator it;
  while(canRead(is,line))
    if((it=_activities.find(line.substr(0,10))) == _activities.end())
      continue;
    else {
      line=line.substr(11);
      while(!line.empty()) {
        int sep=(int)line.find(";");
        if(!const_cast<Activity&>(*it).readCluster(line.substr(0,sep)))
          return false;
        line=line.substr(sep+1);
      }
    }
  return isEmptyStream(is,line);
}
bool Activities::readGlobalTransforms(const string& dir)
{
  for(set<Activity>::const_iterator beg=_activities.begin(),end=_activities.end(); beg!=end; beg++)
    if(!const_cast<Activity&>(*beg).readGlobalFrame(dir+beg->id()+"_globalTransform.txt"))
      return false;
  return true;
}
bool Activities::readFrames(Operation& op)
{
  for(set<Activity>::const_iterator beg=_activities.begin(),end=_activities.end(); beg!=end; beg++) {
    string pathRGBD=_pathRGBD.find(beg->id())->second;
    string pathAnnotation=_pathAnnotation.find(beg->id())->second;
    if(!const_cast<Activity&>(*beg).readFrames(pathRGBD,pathAnnotation,op))
      return false;
  }
  return true;
}
void Activities::merge(const Activities& acts)
{
  _activities.insert(acts._activities.begin(), acts._activities.end());
  _classMap.insert(acts._classMap.begin(), acts._classMap.end());
  _affMap.insert(acts._affMap.begin(), acts._affMap.end());
}
Activity Activities::getActivity(const string& id) const
{
  set<Activity>::const_iterator it=_activities.find(Activity(id));
  if(it != _activities.end())
    return *it;
  ASSERT(false)
  return Activity();
}
const set<Activity>& Activities::getActivities() const
{
  return _activities;
}
//WriteOperation
WriteOperation::WriteOperation(int interval):_interval(interval)
{
  ASSERT(interval >= 1)
}
void WriteOperation::init(const Activity& act)
{
  _path="./visData"+act.id();
  if(boost::filesystem::exists(_path))
    boost::filesystem::remove_all(_path);
  boost::filesystem::create_directory(_path);
  boost::filesystem::create_directory(_path+"/skel");
  boost::filesystem::create_directory(_path+"/RGBD");
}
void WriteOperation::onActivity(const Activity& act,const RGBDImage& img)
{
  if(img.id()%_interval == 0) {
    int fid=img.id();
    const Skeleton* skel=act.getSkel(fid);
    if(skel)
      skel->writeVTK(_path+"/skel");
    if(fid)
      img.writePCVTK(_path+"/RGBD",act.getObjectFeats(fid));
  }
}
