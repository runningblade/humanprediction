#include "DataStructure.h"
#include "CRF.h"

int main(int argc,char** argv)
{
//#define DEBUG_LP_QP
#ifdef DEBUG_LP_QP
  //LP::runExample();
  QP::runExample();
  exit(EXIT_FAILURE);
#endif

  //main program
  if(argc < 2) {
    cout << "Usage: [exe] [DataLocation]" << endl;
    cout << "Specify the CAD120 dataset directory" << endl;
    cout << "Subject 1/3/4/5 will be read" << endl;
    exit(EXIT_FAILURE);
  }
//#define VISUALIZE
#ifdef VISUALIZE
  WriteOperation op;
  bool force=true;
#else
  Operation op;
  bool force=false;
#endif
  vector<string> prefixesTrain = {
    "Subject1",
    "Subject3",
    "Subject4",
  };
  vector<string> prefixesTest = {
    "Subject5",
  };
  Activities dsTrain, dsTest;
  string dataLoc(argv[1]);
  for(int i=0;i<prefixesTrain.size();i++) {
    Activities ds;
    string prefix(prefixesTrain[i]);
    if(!boost::filesystem::exists(dataLoc+prefix+".bin") || force) {
      ASSERT(ds.readDataActMap(dataLoc+prefix))
      ASSERT(ds.readLabelFile(dataLoc+prefix))
      ASSERT(ds.readSegmentsFile(dataLoc+"Segmentation.txt"))
      ASSERT(ds.readFrames(op))
      ds.write(dataLoc+prefix+".bin");
    }else ds.read(dataLoc+prefix+".bin");
    dsTrain.merge(ds);
  }
  for(int i=0;i<prefixesTest.size();i++) {
    Activities ds;
    string prefix(prefixesTest[i]);
    if(!boost::filesystem::exists(dataLoc+prefix+".bin") || force) {
      ASSERT(ds.readDataActMap(dataLoc+prefix))
      ASSERT(ds.readLabelFile(dataLoc+prefix))
      ASSERT(ds.readSegmentsFile(dataLoc+"Segmentation.txt"))
      ASSERT(ds.readFrames(op))
      ds.write(dataLoc+prefix+".bin");
    }else ds.read(dataLoc+prefix+".bin");
    dsTest.merge(ds);
  }

  //training CRF
//#define CRF_TRAIN
#ifdef CRF_TRAIN
  CRF crf;
  cout << "CRF initializing" << endl;
  crf.init(dsTrain);
  cout << "Training CRF" << endl;
  crf.train(0.01f,1E-5f,dataLoc+"CRF.bin",dsTrain);
  exit(EXIT_SUCCESS);
#endif

  //predicting CRF
#ifndef CRF_TRAIN
  CRF crf;
  cout << "CRF initializing" << endl;
  crf.read(dataLoc+"CRF.bin");
  const set<Activity>& acts = dsTest.getActivities();
  for(set<Activity>::const_iterator it=acts.begin(),end=acts.end();it!=end;it++) {
    cout << "Activity " << it->id() << endl;
    Activity result = *it;
    //predict activity/affordance labels
    crf.predicate(result);
    //evaluate the result with ground truth
  }
  exit(EXIT_SUCCESS);
#endif

  return 0;
}
