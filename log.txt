CRF initializing
Activity 0126141638
GLPK Simplex Optimizer, v4.59
46116 rows, 15908 columns, 108044 non-zeros
      0: obj =  -0.000000000e+00 inf =   4.800e+01 (48)
     97: obj =  -2.398690141e+02 inf =   0.000e+00 (0)
*   500: obj =   6.378371696e+02 inf =   5.851e-13 (1612) 2
*  1000: obj =   1.163592803e+03 inf =   1.388e-15 (1276) 4
*  1500: obj =   1.300791093e+03 inf =   2.537e-14 (882) 1
*  2000: obj =   1.372620928e+03 inf =   8.987e-14 (424) 1
*  2497: obj =   1.380201559e+03 inf =   5.595e-13 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 2.14479  Memory usage: 27076277 bytes
Activity 0126141850
GLPK Simplex Optimizer, v4.59
46116 rows, 15908 columns, 108044 non-zeros
      0: obj =  -0.000000000e+00 inf =   4.800e+01 (48)
     97: obj =  -2.377821660e+02 inf =   0.000e+00 (0)
*   500: obj =   7.571850434e+02 inf =   2.185e-15 (1618) 2
*  1000: obj =   1.250378388e+03 inf =   6.870e-15 (1133) 2
*  1500: obj =   1.366507925e+03 inf =   6.445e-14 (717) 1
*  2000: obj =   1.406038654e+03 inf =   8.512e-15 (288) 1
*  2297: obj =   1.408738075e+03 inf =   1.046e-14 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 1.92776  Memory usage: 27076277 bytes
Activity 0126142037
GLPK Simplex Optimizer, v4.59
50092 rows, 17278 columns, 117358 non-zeros
      0: obj =  -0.000000000e+00 inf =   5.200e+01 (52)
    105: obj =  -2.550994170e+02 inf =   0.000e+00 (0)
*   500: obj =   5.240296811e+02 inf =   1.943e-13 (1823) 2
*  1000: obj =   1.268834094e+03 inf =   1.901e-15 (1480) 4
*  1500: obj =   1.474584671e+03 inf =   2.617e-14 (1162) 2
*  2000: obj =   1.568491844e+03 inf =   3.970e-14 (689) 1
*  2500: obj =   1.602993233e+03 inf =   3.604e-14 (209)
*  2712: obj =   1.603937699e+03 inf =   3.603e-14 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 2.4987  Memory usage: 29342429 bytes
Activity 0126142253
GLPK Simplex Optimizer, v4.59
54068 rows, 18648 columns, 126672 non-zeros
      0: obj =  -0.000000000e+00 inf =   5.600e+01 (56)
    113: obj =  -2.765544308e+02 inf =   0.000e+00 (0)
*   500: obj =   5.373590770e+02 inf =   6.623e-16 (1926) 3
*  1000: obj =   1.039361942e+03 inf =   0.000e+00 (1652) 3
*  1500: obj =   1.381765229e+03 inf =   2.771e-15 (1503) 4
*  2000: obj =   1.605836317e+03 inf =   1.482e-14 (1240) 3
*  2500: obj =   1.705944503e+03 inf =   0.000e+00 (800) 1
*  3000: obj =   1.745409286e+03 inf =   0.000e+00 (324)
*  3342: obj =   1.748319721e+03 inf =   1.256e-15 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 3.57281  Memory usage: 32214949 bytes
Activity 0126143115
GLPK Simplex Optimizer, v4.59
22026 rows, 7672 columns, 51664 non-zeros
      0: obj =  -0.000000000e+00 inf =   3.000e+01 (30)
     61: obj =  -1.697157551e+02 inf =   0.000e+00 (0)
*   500: obj =   7.046690470e+02 inf =   6.966e-14 (607) 2
*  1000: obj =   8.031491672e+02 inf =   3.505e-15 (197) 1
*  1194: obj =   8.070031162e+02 inf =   4.448e-15 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.487623  Memory usage: 12965723 bytes
Activity 0126143251
GLPK Simplex Optimizer, v4.59
22026 rows, 7672 columns, 51664 non-zeros
      0: obj =  -0.000000000e+00 inf =   3.000e+01 (30)
     61: obj =  -1.680287940e+02 inf =   0.000e+00 (0)
*   500: obj =   6.913824312e+02 inf =   2.615e-15 (670) 2
*  1000: obj =   8.243731256e+02 inf =   3.511e-15 (247) 1
*  1247: obj =   8.300466844e+02 inf =   6.106e-16 (0) 1
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.515325  Memory usage: 12965723 bytes
Activity 0126143431
GLPK Simplex Optimizer, v4.59
26664 rows, 9284 columns, 62540 non-zeros
      0: obj =  -0.000000000e+00 inf =   3.600e+01 (36)
     73: obj =  -2.007344299e+02 inf =   0.000e+00 (0)
*   500: obj =   6.188361322e+02 inf =   2.789e-14 (922) 2
*  1000: obj =   8.895397607e+02 inf =   6.037e-15 (565) 3
*  1500: obj =   9.440041373e+02 inf =   2.233e-15 (83)
*  1583: obj =   9.440806172e+02 inf =   1.182e-14 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.816768  Memory usage: 15920601 bytes
Activity 0129111131
GLPK Simplex Optimizer, v4.59
64687 rows, 22182 columns, 151446 non-zeros
      0: obj =  -0.000000000e+00 inf =   5.500e+01 (55)
    111: obj =  -2.380730136e+02 inf =   0.000e+00 (0)
*   500: obj =   6.204025717e+02 inf =   5.543e-14 (2131) 2
*  1000: obj =   1.446040569e+03 inf =   1.684e-13 (1783) 3
*  1500: obj =   1.680869010e+03 inf =   1.428e-15 (1356) 1
*  2000: obj =   1.765701431e+03 inf =   6.455e-14 (901) 1
*  2500: obj =   1.801856770e+03 inf =   3.275e-14 (415)
*  2918: obj =   1.805480559e+03 inf =   5.405e-14 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 3.59676  Memory usage: 38238384 bytes
Activity 0129112015
GLPK Simplex Optimizer, v4.59
64687 rows, 22182 columns, 151446 non-zeros
      0: obj =  -0.000000000e+00 inf =   5.500e+01 (55)
    111: obj =  -2.414719543e+02 inf =   0.000e+00 (0)
*   500: obj =   1.040574707e+03 inf =   2.617e-14 (1966) 2
*  1000: obj =   2.055080791e+03 inf =   4.644e-15 (1593) 3
*  1500: obj =   2.450978022e+03 inf =   4.344e-15 (1365) 3
*  2000: obj =   2.693595870e+03 inf =   1.516e-14 (984) 1
*  2500: obj =   2.809027890e+03 inf =   9.714e-16 (567) 1
*  3000: obj =   2.837471297e+03 inf =   3.551e-14 (98) 1
*  3097: obj =   2.837471297e+03 inf =   5.677e-14 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 3.81052  Memory usage: 38238384 bytes
Activity 0129112226
GLPK Simplex Optimizer, v4.59
30212 rows, 10428 columns, 70788 non-zeros
      0: obj =  -0.000000000e+00 inf =   3.200e+01 (32)
     65: obj =  -1.645822968e+02 inf =   0.000e+00 (0)
*   500: obj =   1.156982497e+03 inf =   1.083e-14 (966) 2
*  1000: obj =   1.474123241e+03 inf =   4.566e-15 (654) 3
*  1500: obj =   1.531397955e+03 inf =   2.996e-14 (209) 1
*  1714: obj =   1.534568377e+03 inf =   5.515e-14 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.99542  Memory usage: 17925333 bytes
Activity 0129112342
GLPK Simplex Optimizer, v4.59
30212 rows, 10428 columns, 70788 non-zeros
      0: obj =  -0.000000000e+00 inf =   3.200e+01 (32)
     65: obj =  -1.586670075e+02 inf =   0.000e+00 (0)
*   500: obj =   7.459659122e+02 inf =   2.028e-13 (903) 2
*  1000: obj =   9.767945452e+02 inf =   1.285e-14 (662) 3
*  1500: obj =   1.047272358e+03 inf =   2.496e-13 (256) 1
*  1783: obj =   1.052129538e+03 inf =   1.798e-13 (0) 1
OPTIMAL LP SOLUTION FOUND
Time elapsed: 1.06672  Memory usage: 17925333 bytes
Activity 0129112522
GLPK Simplex Optimizer, v4.59
64687 rows, 22182 columns, 151446 non-zeros
      0: obj =  -0.000000000e+00 inf =   5.500e+01 (55)
    111: obj =  -2.389927499e+02 inf =   0.000e+00 (0)
*   500: obj =   1.104748049e+03 inf =   2.879e-14 (1929) 2
*  1000: obj =   1.862916511e+03 inf =   2.087e-14 (1734) 4
*  1500: obj =   2.077974888e+03 inf =   0.000e+00 (1511) 4
*  2000: obj =   2.304128530e+03 inf =   1.566e-14 (1304) 3
*  2500: obj =   2.526291935e+03 inf =   7.124e-16 (1040) 3
*  3000: obj =   2.649134902e+03 inf =   1.913e-15 (580)
*  3500: obj =   2.677014429e+03 inf =   2.626e-14 (110) 1
*  3613: obj =   2.677014429e+03 inf =   3.102e-14 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 4.802  Memory usage: 38238384 bytes
Activity 0129112630
GLPK Simplex Optimizer, v4.59
64687 rows, 22182 columns, 151446 non-zeros
      0: obj =  -0.000000000e+00 inf =   5.500e+01 (55)
    111: obj =  -2.443753411e+02 inf =   0.000e+00 (0)
*   500: obj =   6.210325271e+02 inf =   2.213e-14 (2153) 2
*  1000: obj =   1.177999052e+03 inf =   1.950e-13 (1947) 4
*  1500: obj =   1.593887465e+03 inf =   2.320e-14 (1664) 3
*  2000: obj =   1.715621535e+03 inf =   7.409e-14 (1231) 1
*  2500: obj =   1.794840287e+03 inf =   4.638e-14 (767)
*  3000: obj =   1.818308310e+03 inf =   5.697e-14 (270) 1
*  3271: obj =   1.818962332e+03 inf =   5.590e-14 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 4.21058  Memory usage: 38238384 bytes
Activity 0129114054
GLPK Simplex Optimizer, v4.59
22026 rows, 7672 columns, 51664 non-zeros
      0: obj =  -0.000000000e+00 inf =   3.000e+01 (30)
     61: obj =  -1.811331316e+02 inf =   0.000e+00 (0)
*   500: obj =   9.088968554e+02 inf =   1.193e-14 (685) 2
*  1000: obj =   1.124747957e+03 inf =   2.705e-15 (289) 1
*  1292: obj =   1.134968179e+03 inf =   2.776e-17 (0) 1
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.61442  Memory usage: 12965723 bytes
Activity 0129114153
GLPK Simplex Optimizer, v4.59
17388 rows, 6060 columns, 40788 non-zeros
      0: obj =  -0.000000000e+00 inf =   2.400e+01 (24)
     49: obj =  -1.452210998e+02 inf =   0.000e+00 (0)
*   500: obj =   7.666385987e+02 inf =   6.864e-15 (526) 2
*  1000: obj =   8.767428733e+02 inf =   1.177e-15 (77) 1
*  1079: obj =   8.767867357e+02 inf =   7.633e-16 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.396425  Memory usage: 10274877 bytes
Activity 0129114356
GLPK Simplex Optimizer, v4.59
19707 rows, 6866 columns, 46226 non-zeros
      0: obj =  -0.000000000e+00 inf =   2.700e+01 (27)
     55: obj =  -1.661184251e+02 inf =   0.000e+00 (0)
*   500: obj =   6.837108636e+02 inf =   0.000e+00 (681) 3
*  1000: obj =   9.604188312e+02 inf =   1.480e-16 (311) 2
*  1330: obj =   9.811907733e+02 inf =   1.411e-14 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.530193  Memory usage: 11649916 bytes
Activity 0504232829
GLPK Simplex Optimizer, v4.59
2550 rows, 914 columns, 6002 non-zeros
      0: obj =  -0.000000000e+00 inf =   6.000e+00 (6)
     13: obj =  -4.945652333e+01 inf =   0.000e+00 (0)
*   193: obj =   1.899554190e+02 inf =   6.967e-14 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.0134282  Memory usage: 1515367 bytes
Activity 0504233253
GLPK Simplex Optimizer, v4.59
2550 rows, 914 columns, 6002 non-zeros
      0: obj =  -0.000000000e+00 inf =   6.000e+00 (6)
     13: obj =  -4.906654494e+01 inf =   0.000e+00 (0)
*   171: obj =   1.872133050e+02 inf =   2.303e-14 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.00977898  Memory usage: 1515367 bytes
Activity 0504233320
GLPK Simplex Optimizer, v4.59
3644 rows, 1300 columns, 8572 non-zeros
      0: obj =  -0.000000000e+00 inf =   8.000e+00 (8)
     17: obj =  -6.514303835e+01 inf =   0.000e+00 (0)
*   241: obj =   2.256738791e+02 inf =   3.840e-14 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.0199652  Memory usage: 2181741 bytes
Activity 0504235245
GLPK Simplex Optimizer, v4.59
3644 rows, 1300 columns, 8572 non-zeros
      0: obj =  -0.000000000e+00 inf =   8.000e+00 (8)
     17: obj =  -7.330206567e+01 inf =   0.000e+00 (0)
*   279: obj =   2.407958868e+02 inf =   2.609e-15 (0) 1
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.024147  Memory usage: 2181741 bytes
Activity 0504235647
GLPK Simplex Optimizer, v4.59
2550 rows, 914 columns, 6002 non-zeros
      0: obj =  -0.000000000e+00 inf =   6.000e+00 (6)
     13: obj =  -4.318966864e+01 inf =   0.000e+00 (0)
*   207: obj =   1.753120160e+02 inf =   4.103e-13 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.0126228  Memory usage: 1515367 bytes
Activity 0504235908
GLPK Simplex Optimizer, v4.59
3644 rows, 1300 columns, 8572 non-zeros
      0: obj =  -0.000000000e+00 inf =   8.000e+00 (8)
     17: obj =  -7.646721686e+01 inf =   0.000e+00 (0)
*   255: obj =   2.290830376e+02 inf =   1.700e-13 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.020982  Memory usage: 2181741 bytes
Activity 0505002750
GLPK Simplex Optimizer, v4.59
17388 rows, 6060 columns, 40788 non-zeros
      0: obj =  -0.000000000e+00 inf =   2.400e+01 (24)
     49: obj =  -1.236252956e+02 inf =   0.000e+00 (0)
*   500: obj =   8.688824870e+02 inf =   2.947e-14 (460) 2
*   998: obj =   9.725689851e+02 inf =   1.295e-15 (0) 1
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.370739  Memory usage: 10274877 bytes
Activity 0505002942
GLPK Simplex Optimizer, v4.59
17388 rows, 6060 columns, 40788 non-zeros
      0: obj =  -0.000000000e+00 inf =   2.400e+01 (24)
     49: obj =  -1.250698028e+02 inf =   0.000e+00 (0)
*   500: obj =   8.912964246e+02 inf =   5.551e-15 (434) 2
*  1000: obj =   9.908031739e+02 inf =   4.580e-16 (4) 1
*  1004: obj =   9.908031739e+02 inf =   2.026e-15 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.358441  Memory usage: 10274877 bytes
Activity 0505003237
GLPK Simplex Optimizer, v4.59
17388 rows, 6060 columns, 40788 non-zeros
      0: obj =  -0.000000000e+00 inf =   2.400e+01 (24)
     49: obj =  -1.353575265e+02 inf =   0.000e+00 (0)
*   500: obj =   8.330549880e+02 inf =   1.676e-15 (443) 2
*   988: obj =   9.216332704e+02 inf =   4.441e-16 (0) 1
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.346545  Memory usage: 10274877 bytes
Activity 0511140410
GLPK Simplex Optimizer, v4.59
19707 rows, 6866 columns, 46226 non-zeros
      0: obj =  -0.000000000e+00 inf =   2.700e+01 (27)
     55: obj =  -1.582168979e+02 inf =   0.000e+00 (0)
*   500: obj =   6.444778550e+02 inf =   1.891e-15 (655) 3
*  1000: obj =   8.077032667e+02 inf =   7.166e-15 (342) 2
*  1355: obj =   8.287495681e+02 inf =   4.219e-15 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.538311  Memory usage: 11649916 bytes
Activity 0511140450
GLPK Simplex Optimizer, v4.59
22026 rows, 7672 columns, 51664 non-zeros
      0: obj =  -0.000000000e+00 inf =   3.000e+01 (30)
     61: obj =  -1.728155589e+02 inf =   0.000e+00 (0)
*   500: obj =   6.802500615e+02 inf =   1.093e-14 (736) 2
*  1000: obj =   8.411404700e+02 inf =   3.469e-16 (286) 2
*  1296: obj =   8.493009938e+02 inf =   0.000e+00 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.549224  Memory usage: 12965723 bytes
Activity 0511140553
GLPK Simplex Optimizer, v4.59
19707 rows, 6866 columns, 46226 non-zeros
      0: obj =  -0.000000000e+00 inf =   2.700e+01 (27)
     55: obj =  -1.570518779e+02 inf =   0.000e+00 (0)
*   500: obj =   7.664765336e+02 inf =   1.977e-14 (604) 2
*  1000: obj =   9.150566765e+02 inf =   1.306e-14 (170) 1
*  1171: obj =   9.175715922e+02 inf =   6.679e-14 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.443563  Memory usage: 11649916 bytes
Activity 0511141007
GLPK Simplex Optimizer, v4.59
16772 rows, 5932 columns, 39412 non-zeros
      0: obj =  -0.000000000e+00 inf =   3.200e+01 (32)
     65: obj =  -2.667757790e+02 inf =   0.000e+00 (0)
*   500: obj =   6.577153960e+02 inf =   5.371e-15 (572) 2
*  1000: obj =   7.500360767e+02 inf =   0.000e+00 (107) 1
*  1107: obj =   7.500360767e+02 inf =   0.000e+00 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.347042  Memory usage: 9938037 bytes
Activity 0511141231
GLPK Simplex Optimizer, v4.59
23336 rows, 8248 columns, 54832 non-zeros
      0: obj =  -0.000000000e+00 inf =   4.400e+01 (44)
     89: obj =  -3.833930440e+02 inf =   0.000e+00 (0)
*   500: obj =   8.121754906e+02 inf =   3.046e-14 (952) 2
*  1000: obj =   1.122529407e+03 inf =   1.529e-14 (653) 3
*  1500: obj =   1.149988739e+03 inf =   0.000e+00 (203) 1
*  1703: obj =   1.149988739e+03 inf =   0.000e+00 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.75584  Memory usage: 13732985 bytes
Activity 0511141338
GLPK Simplex Optimizer, v4.59
18960 rows, 6704 columns, 44552 non-zeros
      0: obj =  -0.000000000e+00 inf =   3.600e+01 (36)
     73: obj =  -3.156171675e+02 inf =   0.000e+00 (0)
*   500: obj =   8.125526033e+02 inf =   2.776e-17 (667) 2
*  1000: obj =   9.657036875e+02 inf =   2.720e-15 (227) 1
*  1228: obj =   9.657036875e+02 inf =   5.551e-17 (0)
OPTIMAL LP SOLUTION FOUND
Time elapsed: 0.437278  Memory usage: 11237153 bytes
